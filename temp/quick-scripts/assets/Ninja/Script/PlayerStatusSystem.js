(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/Ninja/Script/PlayerStatusSystem.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '18442TA0/tAq4T6U/TGgFsM', 'PlayerStatusSystem', __filename);
// Ninja/Script/PlayerStatusSystem.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var PlayerStatusSystem = /** @class */ (function (_super) {
    __extends(PlayerStatusSystem, _super);
    function PlayerStatusSystem() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.HP = 5;
        _this.Energy = 5;
        _this.HPBar = null;
        _this.EnergyBar = null;
        _this.HPPrefab = null;
        _this.EnergyPrefab = null;
        _this.HPList = [];
        _this.EnergyList = [];
        _this.CurrentHP = 0;
        _this.CurrentEnergy = 0;
        _this.isDead = false;
        _this.Timer = 0;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    PlayerStatusSystem.prototype.start = function () {
        this.Init();
    };
    PlayerStatusSystem.prototype.Init = function () {
        this.CurrentHP = this.HP;
        this.CurrentEnergy = this.Energy;
        var Offset = 0;
        for (var i = 0; i < this.CurrentEnergy; i++) {
            var energy = cc.instantiate(this.EnergyPrefab);
            energy.parent = this.EnergyBar;
            energy.position = cc.v2(Offset, 0);
            this.EnergyList.push(energy);
            Offset += 35;
        }
        Offset = 0;
        for (var i = 0; i < this.CurrentHP; i++) {
            var hp = cc.instantiate(this.HPPrefab);
            hp.parent = this.HPBar;
            hp.position = cc.v2(-Offset, 0);
            this.HPList.push(hp);
            Offset += 35;
        }
    };
    PlayerStatusSystem.prototype.changeHP = function (amount) {
        this.CurrentHP += amount;
        if (this.CurrentHP >= this.HP) {
            this.CurrentHP = this.HP;
        }
        else if (this.CurrentHP <= 0) {
            this.CurrentHP = 0;
        }
        while (this.HPList.length != this.CurrentHP) {
            if (this.HPList.length < this.CurrentHP) {
                var Offset = this.HPList.length * 35;
                var hp = cc.instantiate(this.HPPrefab);
                hp.parent = this.HPBar;
                hp.position = cc.v2(-Offset, 0);
                this.HPList.push(hp);
            }
            else if (this.HPList.length > this.CurrentHP) {
                var hp = this.HPList.pop();
                hp.destroy();
            }
        }
    };
    PlayerStatusSystem.prototype.changeEnergy = function (amount) {
        this.CurrentEnergy += amount;
        if (this.CurrentEnergy >= this.Energy) {
            this.CurrentEnergy = this.Energy;
        }
        else if (this.CurrentEnergy <= 0) {
            this.CurrentEnergy = 0;
        }
        while (this.EnergyList.length != this.CurrentEnergy) {
            if (this.EnergyList.length < this.CurrentEnergy) {
                var Offset = this.EnergyList.length * 35;
                var energy = cc.instantiate(this.EnergyPrefab);
                energy.parent = this.EnergyBar;
                energy.position = cc.v2(Offset, 0);
                this.EnergyList.push(energy);
            }
            else if (this.EnergyList.length > this.CurrentEnergy) {
                var energy = this.EnergyList.pop();
                energy.destroy();
            }
        }
    };
    PlayerStatusSystem.prototype.update = function (dt) {
        if (this.CurrentHP == 0) {
            this.isDead = true;
        }
        else {
            this.Timer += dt;
            if (this.Timer >= 5) {
                this.Timer = 0;
                this.changeEnergy(1);
            }
        }
    };
    __decorate([
        property
    ], PlayerStatusSystem.prototype, "HP", void 0);
    __decorate([
        property
    ], PlayerStatusSystem.prototype, "Energy", void 0);
    __decorate([
        property(cc.Node)
    ], PlayerStatusSystem.prototype, "HPBar", void 0);
    __decorate([
        property(cc.Node)
    ], PlayerStatusSystem.prototype, "EnergyBar", void 0);
    __decorate([
        property(cc.Prefab)
    ], PlayerStatusSystem.prototype, "HPPrefab", void 0);
    __decorate([
        property(cc.Prefab)
    ], PlayerStatusSystem.prototype, "EnergyPrefab", void 0);
    PlayerStatusSystem = __decorate([
        ccclass
    ], PlayerStatusSystem);
    return PlayerStatusSystem;
}(cc.Component));
exports.default = PlayerStatusSystem;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=PlayerStatusSystem.js.map
        