(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/Ninja/Script/PlayerSoundController.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '21b40y2t7hCgqzYfgXyjnbJ', 'PlayerSoundController', __filename);
// Ninja/Script/PlayerSoundController.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var PlayerSoundController = /** @class */ (function (_super) {
    __extends(PlayerSoundController, _super);
    function PlayerSoundController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.WalkingSE = null;
        _this.JumpSE = null;
        _this.DoubleJumpSE = null;
        _this.AttackSE0 = null;
        _this.AttackSE1 = null;
        _this.AttackSE2 = null;
        _this.AttackSE3 = null;
        _this.AttackSE4 = null;
        _this.AttackSE5 = null;
        _this.AttackSE6 = null;
        _this.AttackSE7 = null;
        _this.AttackDamageSE0 = null;
        _this.AttackDamageSE1 = null;
        _this.AttackDamageSE2 = null;
        _this.AttackDamageSE3 = null;
        _this.ChargeSE1 = null;
        _this.ChargeSE2 = null;
        _this.ChargeSE3 = null;
        _this.ReleaseSE = null;
        _this.HurtSE = null;
        _this.DieSE = null;
        _this.Skill1SE = null;
        _this.Skill2SE = null;
        _this.currentPlaying = "";
        _this.currentID = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    PlayerSoundController.prototype.PlaySoundEffect = function (clip) {
        this.StopCurrentSoundEffect();
        this.currentPlaying = clip;
        if (clip == "Walking") {
            this.currentID = cc.audioEngine.playEffect(this.WalkingSE, true);
        }
        else if (clip == "Jump") {
            this.currentID = cc.audioEngine.playEffect(this.JumpSE, false);
        }
        else if (clip == "DoubleJump") {
            this.currentID = cc.audioEngine.playEffect(this.DoubleJumpSE, false);
        }
        else if (clip == "Attack") {
            var num = Math.ceil(Math.random() * 8);
            var AttackSE = null;
            if (num == 1)
                AttackSE = this.AttackSE0;
            else if (num == 2)
                AttackSE = this.AttackSE1;
            else if (num == 3)
                AttackSE = this.AttackSE2;
            else if (num == 4)
                AttackSE = this.AttackSE3;
            else if (num == 5)
                AttackSE = this.AttackSE4;
            else if (num == 6)
                AttackSE = this.AttackSE5;
            else if (num == 7)
                AttackSE = this.AttackSE6;
            else
                AttackSE = this.AttackSE7;
            this.currentID = cc.audioEngine.playEffect(AttackSE, false);
        }
        else if (clip == "Hit") {
            var num = Math.ceil(Math.random() * 4);
            var HitSE = null;
            if (num == 1)
                HitSE = this.AttackDamageSE0;
            else if (num == 2)
                HitSE = this.AttackDamageSE1;
            else if (num == 3)
                HitSE = this.AttackDamageSE2;
            else
                HitSE = this.AttackDamageSE3;
            this.currentID = cc.audioEngine.playEffect(HitSE, false);
        }
        else if (clip == "Charge") {
            this.currentID = cc.audioEngine.playEffect(this.ChargeSE2, false);
        }
        else if (clip == "Release") {
            this.currentID = cc.audioEngine.playEffect(this.ReleaseSE, false);
        }
        else if (clip == "Hurt") {
            this.currentID = cc.audioEngine.playEffect(this.HurtSE, false);
        }
        else if (clip == "Die") {
            this.currentID = cc.audioEngine.playEffect(this.DieSE, false);
        }
        else if (clip == "Skill1") {
            this.currentID = cc.audioEngine.playEffect(this.Skill1SE, false);
        }
        else if (clip == "Skill2") {
            this.currentID = cc.audioEngine.playEffect(this.Skill2SE, false);
        }
    };
    PlayerSoundController.prototype.StopCurrentSoundEffect = function () {
        // stop current playing sound effect
        cc.audioEngine.stopEffect(this.currentID);
        this.currentPlaying = null;
    };
    PlayerSoundController.prototype.IsCurrentPlaying = function (clip) {
        if (this.currentPlaying == clip) {
            return true;
        }
        else {
            return false;
        }
    };
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "WalkingSE", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "JumpSE", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "DoubleJumpSE", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "AttackSE0", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "AttackSE1", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "AttackSE2", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "AttackSE3", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "AttackSE4", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "AttackSE5", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "AttackSE6", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "AttackSE7", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "AttackDamageSE0", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "AttackDamageSE1", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "AttackDamageSE2", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "AttackDamageSE3", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "ChargeSE1", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "ChargeSE2", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "ChargeSE3", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "ReleaseSE", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "HurtSE", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "DieSE", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "Skill1SE", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], PlayerSoundController.prototype, "Skill2SE", void 0);
    PlayerSoundController = __decorate([
        ccclass
    ], PlayerSoundController);
    return PlayerSoundController;
}(cc.Component));
exports.default = PlayerSoundController;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=PlayerSoundController.js.map
        