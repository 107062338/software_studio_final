(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/Script/animals/hawk.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '857b6P0krlNIodmu1uRPJS1', 'hawk', __filename);
// Script/animals/hawk.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Hawk = /** @class */ (function (_super) {
    __extends(Hawk, _super);
    function Hawk() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.Player = null;
        _this.distance = 400;
        _this.speed = 100;
        _this.charge = null;
        _this.death_particle = null;
        _this.attack_sound = null;
        _this.width = 300;
        _this.height = 300;
        _this.x_offset = 0;
        _this.y_offset = 0;
        _this.state = 'stop';
        _this.anim = null;
        _this.fly_action = null;
        _this.attack_action = null;
        _this.attack_enabled = true;
        _this.behavior = '';
        _this.direction = false;
        _this.left_bound = 0;
        _this.right_bound = 0;
        _this.origin_y = 0;
        _this.detect_left = 0;
        _this.detect_right = 0;
        _this.detect_top = 0;
        _this.detect_bottom = 0;
        return _this;
    }
    Hawk.prototype.start = function () {
        this.anim = this.getComponent(cc.Animation);
        this.left_bound = this.node.x - this.distance;
        this.right_bound = this.node.x + this.distance;
        this.origin_y = this.node.y;
        this.detect_left = this.node.x - this.width / 2 - this.x_offset;
        this.detect_right = this.node.x + this.width / 2 - this.x_offset;
        this.detect_top = this.node.y + this.height / 2 + this.y_offset;
        this.detect_bottom = this.node.y - this.height / 2 + this.y_offset;
    };
    Hawk.prototype.update = function (dt) {
        if (this.state == 'stop') {
            if (this.Player.y < this.detect_top && this.Player.y > this.detect_bottom) {
                if (this.Player.x < this.detect_right && this.Player.x > this.detect_left) {
                    this.state = 'active';
                    this.anim.play('hawk_fly');
                    this.fly_behavior();
                }
            }
        }
        if (this.state == 'active' && this.behavior != 'death') {
            if (this.Player.y > this.detect_top || this.Player.y < this.detect_bottom || this.Player.x > this.detect_right || this.Player.x < this.detect_left) {
                this.anim.stop();
                this.state = 'stop';
                this.attack_enabled = true;
                this.charge.stopSystem();
                this.unscheduleAllCallbacks();
                this.node.stopAllActions();
            }
        }
        if (Math.abs(this.Player.x - this.node.x) < 500 && Math.abs(this.Player.y - this.node.y) < 400 && this.state == 'active' && this.attack_enabled == true) {
            this.attack_behavior();
        }
    };
    Hawk.prototype.fly_behavior = function () {
        if (this.direction == false) {
            this.node.scaleX = -1;
            this.fly_action = cc.sequence(cc.moveTo(Math.abs(this.left_bound - this.node.x) / this.speed, this.left_bound, this.origin_y), cc.callFunc(function () { this.direction = true; this.fly_behavior(); }, this));
        }
        else {
            this.node.scaleX = 1;
            this.fly_action = cc.sequence(cc.moveTo(Math.abs(this.right_bound - this.node.x) / this.speed, this.right_bound, this.origin_y), cc.callFunc(function () { this.direction = false; this.fly_behavior(); }, this));
        }
        this.node.runAction(this.fly_action);
    };
    Hawk.prototype.attack_behavior = function () {
        var _this = this;
        this.attack_enabled = false;
        cc.audioEngine.playEffect(this.attack_sound, false);
        this.node.stopAction(this.fly_action);
        this.charge.resetSystem();
        this.attack_action = cc.sequence(cc.callFunc(function () {
            if (_this.node.x < _this.Player.x)
                _this.node.scaleX = 1;
            else
                _this.node.scaleX = -1;
        }, this), cc.moveTo(1, this.Player.x, this.Player.y), cc.callFunc(function () {
            if (_this.node.scaleX == 1)
                _this.node.scaleX = -1;
            else
                _this.node.scaleX = 1;
        }, this), cc.moveTo(1, this.node.x, this.node.y), cc.callFunc(function () { _this.charge.stopSystem(); _this.fly_behavior(); _this.scheduleOnce(function () { _this.attack_enabled = true; }, 7); }));
        this.node.runAction(this.attack_action);
    };
    Hawk.prototype.onBeginContact = function (contact, self, other) {
        var _this = this;
        /*  Interaction with the Player*/
        if (other.tag == 1000 && this.behavior != 'death') {
            if (other.node.getComponent('PlayerController').isAttacking != 0) {
                this.behavior = 'death';
                this.unscheduleAllCallbacks();
                this.node.stopAllActions();
                this.anim.stop();
                this.charge.stopSystem();
                this.attack_enabled = false;
                this.scheduleOnce(function () { _this.node.runAction(cc.sequence(cc.spawn(cc.fadeOut(3), cc.callFunc(function () { _this.death_particle.resetSystem(); })), cc.callFunc(function () { _this.node.destroy(); }))); }, 1);
            }
        }
        if (other.tag == 1001 && this.behavior != 'death') {
            this.behavior = 'death';
            this.unscheduleAllCallbacks();
            this.node.stopAllActions();
            this.anim.stop();
            this.charge.stopSystem();
            this.attack_enabled = false;
            this.scheduleOnce(function () { _this.node.runAction(cc.sequence(cc.spawn(cc.fadeOut(3), cc.callFunc(function () { _this.death_particle.resetSystem(); })), cc.callFunc(function () { _this.node.destroy(); }))); }, 1);
        }
    };
    __decorate([
        property(cc.Node)
    ], Hawk.prototype, "Player", void 0);
    __decorate([
        property()
    ], Hawk.prototype, "distance", void 0);
    __decorate([
        property()
    ], Hawk.prototype, "speed", void 0);
    __decorate([
        property(cc.ParticleSystem)
    ], Hawk.prototype, "charge", void 0);
    __decorate([
        property(cc.ParticleSystem)
    ], Hawk.prototype, "death_particle", void 0);
    __decorate([
        property(cc.AudioClip)
    ], Hawk.prototype, "attack_sound", void 0);
    __decorate([
        property()
    ], Hawk.prototype, "width", void 0);
    __decorate([
        property()
    ], Hawk.prototype, "height", void 0);
    __decorate([
        property()
    ], Hawk.prototype, "x_offset", void 0);
    __decorate([
        property()
    ], Hawk.prototype, "y_offset", void 0);
    Hawk = __decorate([
        ccclass
    ], Hawk);
    return Hawk;
}(cc.Component));
exports.default = Hawk;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=hawk.js.map
        