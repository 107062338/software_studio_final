(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/Script/animals/deer_male.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '01156DEtwVIxY3ARFQO7D4U', 'deer_male', __filename);
// Script/animals/deer_male.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Deer_male = /** @class */ (function (_super) {
    __extends(Deer_male, _super);
    function Deer_male() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.deer_male_particle = null;
        _this.anim = null;
        return _this;
    }
    Deer_male.prototype.start = function () {
        this.node.runAction(cc.fadeOut(1));
        this.anim = this.getComponent(cc.Animation);
    };
    Deer_male.prototype.deer_male_anim = function () {
        var _this = this;
        this.node.runAction(cc.spawn(cc.callFunc(function () { _this.deer_male_particle.resetSystem(); _this.anim.play('deer_final_male'); }), cc.fadeIn(2), cc.moveBy(1.5, -150, 0)));
        this.scheduleOnce(function () { _this.node.runAction(cc.sequence(cc.spawn(cc.moveBy(2, 150, 0), cc.callFunc(function () { _this.node.scaleX = -1; }), cc.fadeOut(2)), cc.callFunc(function () { _this.node.destroy(); }))); }, 6);
    };
    __decorate([
        property(cc.ParticleSystem)
    ], Deer_male.prototype, "deer_male_particle", void 0);
    Deer_male = __decorate([
        ccclass
    ], Deer_male);
    return Deer_male;
}(cc.Component));
exports.default = Deer_male;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=deer_male.js.map
        