(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/Script/animals/bear.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '9a11512qABHJa+5o+aJ4O6x', 'bear', __filename);
// Script/animals/bear.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Bear = /** @class */ (function (_super) {
    __extends(Bear, _super);
    function Bear() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.Player = null;
        _this.limit = 400;
        _this.death_particle = null;
        _this.attack_particle = null;
        _this.attack_sound = null;
        _this.death_sound = null;
        _this.width = 300;
        _this.height = 300;
        _this.x_offset = 0;
        _this.y_offset = 0;
        _this.anim = null;
        _this.state = 'stop';
        _this.behavior = '';
        _this.hurted_enable = true;
        _this.health = 80;
        _this.direction = 1;
        _this.distance = 0;
        _this.detect_left = 0;
        _this.detect_right = 0;
        _this.detect_top = 0;
        _this.detect_bottom = 0;
        return _this;
    }
    Bear.prototype.start = function () {
        this.anim = this.getComponent(cc.Animation);
        this.detect_left = this.node.x - this.width / 2 - this.x_offset;
        this.detect_right = this.node.x + this.width / 2 - this.x_offset;
        this.detect_top = this.node.y + this.height / 2 + this.y_offset;
        this.detect_bottom = this.node.y - this.height / 2 + this.y_offset;
        this.life_bar = this.node.getChildByName('bear_life');
    };
    Bear.prototype.update = function (dt) {
        if (this.node.scaleX == -1) { //right
            this.life_bar.x = (80 - this.health) / 2;
            this.life_bar.width = this.health;
            this.life_bar.angle = this.node.angle;
        }
        else { //left
            this.life_bar.x = -(80 - this.health) / 2;
            this.life_bar.width = this.health;
            this.life_bar.angle = -this.node.angle;
        }
        if (this.state == 'stop') {
            if (this.Player.y < this.detect_top && this.Player.y > this.detect_bottom) {
                if (this.Player.x < this.detect_right && this.Player.x > this.detect_left) {
                    this.state = 'active';
                    this.idle_behavior();
                }
            }
        }
        if (this.state == 'active' && this.behavior != 'death') {
            if (this.Player.y > this.detect_top || this.Player.y < this.detect_bottom || this.Player.x > this.detect_right || this.Player.x < this.detect_left) {
                this.state = 'stop';
                this.behavior = '';
                this.hurted_enable = true;
                this.unscheduleAllCallbacks();
                this.anim.stop();
            }
        }
        if (this.behavior == 'run') {
            this.node.x += this.direction;
            this.distance += 2 * this.direction;
        }
        else if (this.behavior == 'walk') {
            this.node.x += 0.5 * this.direction;
            this.distance += this.direction;
        }
        if (this.distance >= this.limit) {
            this.unscheduleAllCallbacks();
            this.distance = this.limit - 1;
            this.idle_behavior();
        }
        else if (this.distance <= -this.limit) {
            this.unscheduleAllCallbacks();
            this.distance = -this.limit + 1;
            this.idle_behavior();
        }
    };
    Bear.prototype.idle_behavior = function () {
        var _this = this;
        if (this.hurted_enable == false)
            this.scheduleOnce(function () { _this.hurted_enable = true; }, 0.5);
        var time = Math.random() * 1 + 2.4;
        if (this.behavior != 'idle' && this.behavior != 'death') {
            this.anim.play('bear_idle');
            this.behavior = 'idle';
        }
        if (this.state != 'stop') {
            var random = Math.random();
            if (random > 0.67) {
                this.scheduleOnce(this.run_behavior, time);
            }
            else if (random > 0.33) {
                this.scheduleOnce(this.walk_behavior, time);
            }
            else {
                this.scheduleOnce(this.eat_behavior, time);
            }
        }
    };
    Bear.prototype.run_behavior = function () {
        var time = ((Math.random() * 3) / 1) + 1;
        if (this.behavior != 'run' && this.behavior != 'death') {
            this.anim.play('bear_run');
            this.behavior = 'run';
            if (this.distance >= this.limit - 1) {
                this.direction = -1;
                this.node.scaleX = 1;
            }
            else if (this.distance <= -this.limit + 1) {
                this.direction = 1;
                this.node.scaleX = -1;
            }
            else {
                if (Math.random() > 0.5) {
                    this.direction = -1;
                    this.node.scaleX = 1;
                }
                else {
                    this.direction = 1;
                    this.node.scaleX = -1;
                }
            }
        }
        if (this.state != 'stop') {
            var random = Math.random();
            if (random > 0.67) {
                this.scheduleOnce(this.idle_behavior, time);
            }
            else if (random > 0.33) {
                this.scheduleOnce(this.walk_behavior, time);
            }
            else {
                this.scheduleOnce(this.eat_behavior, time);
            }
        }
    };
    Bear.prototype.walk_behavior = function () {
        var time = ((Math.random() * 4) / 1.5) * 1.5 + 1.5;
        if (this.behavior != 'walk' && this.behavior != 'death') {
            this.anim.play('bear_walk');
            this.behavior = 'walk';
            if (this.distance >= this.limit - 1) {
                this.direction = -1;
                this.node.scaleX = 1;
            }
            else if (this.distance <= -this.limit + 1) {
                this.direction = 1;
                this.node.scaleX = -1;
            }
            else {
                if (Math.random() > 0.5) {
                    this.direction = -1;
                    this.node.scaleX = 1;
                }
                else {
                    this.direction = 1;
                    this.node.scaleX = -1;
                }
            }
        }
        if (this.state != 'stop') {
            var random = Math.random();
            if (random > 0.67) {
                this.scheduleOnce(this.idle_behavior, time);
            }
            else if (random > 0.33) {
                this.scheduleOnce(this.run_behavior, time);
            }
            else {
                this.scheduleOnce(this.eat_behavior, time);
            }
        }
    };
    Bear.prototype.eat_behavior = function () {
        var time = Math.random() + 2.13;
        if (this.behavior != 'eat' && this.behavior != 'death') {
            this.anim.play('bear_eat');
            this.behavior = 'eat';
        }
        if (this.state != 'stop') {
            var random = Math.random();
            if (random > 0.67) {
                this.scheduleOnce(this.idle_behavior, time);
            }
            else if (random > 0.33) {
                this.scheduleOnce(this.walk_behavior, time);
            }
            else {
                this.scheduleOnce(this.run_behavior, time);
            }
        }
    };
    Bear.prototype.hurted_behavior = function (damage) {
        var _this = this;
        if (this.health - damage * 2 > 0) {
            this.schedule(function () {
                _this.health -= 2;
            }, 1 / damage, damage);
            this.scheduleOnce(function () { _this.hurted_enable = true; }, 1);
        }
        else {
            this.schedule(function () {
                if (_this.health > 2)
                    _this.health -= 2;
                else {
                    _this.health = 0;
                    _this.unscheduleAllCallbacks();
                    _this.death_behavior();
                }
            }, 1 / damage, damage);
        }
    };
    Bear.prototype.death_behavior = function () {
        var _this = this;
        if (this.behavior != 'death') {
            this.anim.play('bear_death');
            this.behavior = 'death';
            cc.audioEngine.playEffect(this.death_sound, false);
            this.unscheduleAllCallbacks();
            this.scheduleOnce(function () { _this.node.runAction(cc.sequence(cc.spawn(cc.fadeOut(3), cc.callFunc(function () { _this.death_particle.resetSystem(); })), cc.callFunc(function () { _this.node.destroy(); }))); }, 3.0);
        }
    };
    Bear.prototype.attack_behavior = function (dir) {
        if (this.behavior != 'attack' && this.behavior != 'death') {
            this.anim.play('bear_attack');
            if (dir == 'left')
                this.node.scaleX = 1;
            else
                this.node.scaleX = -1;
            cc.audioEngine.playEffect(this.attack_sound, false);
            this.behavior = 'attack';
            this.unscheduleAllCallbacks();
            this.attack_particle.resetSystem();
        }
        if (this.state != 'stop') {
            this.scheduleOnce(this.walk_behavior, 1.6);
        }
    };
    Bear.prototype.onBeginContact = function (contact, self, other) {
        if (other.node.name == 'rabbit' || other.node.name == 'deer_female' || other.node.name == 'Player') {
            contact.disabled = true;
        }
        if (other.tag == 1000) {
            contact.disabled = true;
            //Interaction with the Player
            if (other.node.getComponent('PlayerController').isAttacking != 0 && this.hurted_enable == true && this.behavior != 'death') {
                this.hurted_enable = false;
                this.hurted_behavior((Math.random() * 20) / 1);
            }
            else if (other.node.getComponent('PlayerController').isAttacking == 0 && this.behavior != 'death') {
                if (other.node.x > this.node.x)
                    this.attack_behavior('right');
                else
                    this.attack_behavior('left');
            }
        }
        else if (other.tag == 1001 && this.hurted_enable == true && this.behavior != 'death') {
            contact.disabled = true;
            this.hurted_enable = false;
            this.hurted_behavior((Math.random() * 20) / 1);
        }
        else if (other.tag == 1002) {
            this.unscheduleAllCallbacks();
            this.hurted_behavior(40);
        }
    };
    __decorate([
        property(cc.Node)
    ], Bear.prototype, "Player", void 0);
    __decorate([
        property()
    ], Bear.prototype, "limit", void 0);
    __decorate([
        property(cc.ParticleSystem)
    ], Bear.prototype, "death_particle", void 0);
    __decorate([
        property(cc.ParticleSystem)
    ], Bear.prototype, "attack_particle", void 0);
    __decorate([
        property(cc.AudioClip)
    ], Bear.prototype, "attack_sound", void 0);
    __decorate([
        property(cc.AudioClip)
    ], Bear.prototype, "death_sound", void 0);
    __decorate([
        property()
    ], Bear.prototype, "width", void 0);
    __decorate([
        property()
    ], Bear.prototype, "height", void 0);
    __decorate([
        property()
    ], Bear.prototype, "x_offset", void 0);
    __decorate([
        property()
    ], Bear.prototype, "y_offset", void 0);
    Bear = __decorate([
        ccclass
    ], Bear);
    return Bear;
}(cc.Component));
exports.default = Bear;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=bear.js.map
        