(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/Script/animals/bat.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '7426d2a2rFL+JRdwX/7dKc2', 'bat', __filename);
// Script/animals/bat.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Bat = /** @class */ (function (_super) {
    __extends(Bat, _super);
    function Bat() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.Player = null;
        _this.distance = 400;
        _this.speed = 100;
        _this.width = 300;
        _this.height = 300;
        _this.x_offset = 0;
        _this.y_offset = 0;
        _this.state = 'stop';
        _this.anim = null;
        _this.fly_action = null;
        _this.detect_left = 0;
        _this.detect_right = 0;
        _this.detect_top = 0;
        _this.detect_bottom = 0;
        return _this;
    }
    Bat.prototype.start = function () {
        this.anim = this.getComponent(cc.Animation);
        this.fly_action = cc.repeatForever(cc.sequence(cc.callFunc(function () { this.node.scaleX = -1; }, this), cc.moveBy(this.distance / this.speed, -this.distance, 0), cc.callFunc(function () { this.node.scaleX = 1; }, this), cc.moveBy(this.distance * 2 / this.speed, this.distance * 2, 0)));
        this.detect_left = this.node.x - this.width / 2 - this.x_offset;
        this.detect_right = this.node.x + this.width / 2 - this.x_offset;
        this.detect_top = this.node.y + this.height / 2 + this.y_offset;
        this.detect_bottom = this.node.y - this.height / 2 + this.y_offset;
    };
    Bat.prototype.update = function () {
        if (this.state == 'stop') {
            if (this.Player.y < this.detect_top && this.Player.y > this.detect_bottom) {
                if (this.Player.x < this.detect_right && this.Player.x > this.detect_left) {
                    this.anim.play('bat_fly');
                    this.state = 'active';
                    this.node.runAction(this.fly_action);
                }
            }
        }
        if (this.state == 'active') {
            if (this.Player.y > this.detect_top || this.Player.y < this.detect_bottom || this.Player.x > this.detect_right || this.Player.x < this.detect_left) {
                this.state = 'stop';
                this.anim.stop();
                this.node.stopAllActions();
            }
        }
    };
    __decorate([
        property(cc.Node)
    ], Bat.prototype, "Player", void 0);
    __decorate([
        property()
    ], Bat.prototype, "distance", void 0);
    __decorate([
        property()
    ], Bat.prototype, "speed", void 0);
    __decorate([
        property()
    ], Bat.prototype, "width", void 0);
    __decorate([
        property()
    ], Bat.prototype, "height", void 0);
    __decorate([
        property()
    ], Bat.prototype, "x_offset", void 0);
    __decorate([
        property()
    ], Bat.prototype, "y_offset", void 0);
    Bat = __decorate([
        ccclass
    ], Bat);
    return Bat;
}(cc.Component));
exports.default = Bat;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=bat.js.map
        