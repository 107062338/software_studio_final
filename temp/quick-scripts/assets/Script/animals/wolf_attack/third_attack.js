(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/Script/animals/wolf_attack/third_attack.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '108e8abG6dPT7PgbTdbTYDH', 'third_attack', __filename);
// Script/animals/wolf_attack/third_attack.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Thirt_attack = /** @class */ (function (_super) {
    __extends(Thirt_attack, _super);
    function Thirt_attack() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.position0 = null;
        _this.position1 = null;
        _this.position2 = null;
        _this.position3 = null;
        _this.position4 = null;
        return _this;
        // update (dt) {}
    }
    // LIFE-CYCLE CALLBACKS:
    Thirt_attack.prototype.onLoad = function () {
    };
    Thirt_attack.prototype.start = function () {
        var _this = this;
        this.position0.getComponent(cc.PhysicsBoxCollider).enabled = true;
        if (Math.random() > 0.5)
            this.position0.skewX = Math.random() * 30;
        else
            this.position0.skewX = Math.random() * -30;
        if (Math.random() > 0.5)
            this.position0.getComponent(cc.Animation).play('electricityanim1');
        else
            this.position0.getComponent(cc.Animation).play('electricityanim7');
        this.scheduleOnce(function () { _this.position0.destroy(); }, 0.8);
        this.position1.getComponent(cc.PhysicsBoxCollider).enabled = true;
        if (Math.random() > 0.5)
            this.position1.skewX = Math.random() * 30;
        else
            this.position1.skewX = Math.random() * -30;
        if (Math.random() > 0.5)
            this.position1.getComponent(cc.Animation).play('electricityanim1');
        else
            this.position1.getComponent(cc.Animation).play('electricityanim7');
        this.scheduleOnce(function () { _this.position1.destroy(); }, 0.8);
        this.position2.getComponent(cc.PhysicsBoxCollider).enabled = true;
        if (Math.random() > 0.5)
            this.position2.skewX = Math.random() * 30;
        else
            this.position2.skewX = Math.random() * -30;
        if (Math.random() > 0.5)
            this.position2.getComponent(cc.Animation).play('electricityanim1');
        else
            this.position2.getComponent(cc.Animation).play('electricityanim7');
        this.scheduleOnce(function () { _this.position2.destroy(); }, 0.8);
        this.position3.getComponent(cc.PhysicsBoxCollider).enabled = true;
        if (Math.random() > 0.5)
            this.position3.skewX = Math.random() * 30;
        else
            this.position3.skewX = Math.random() * -30;
        if (Math.random() > 0.5)
            this.position3.getComponent(cc.Animation).play('electricityanim1');
        else
            this.position3.getComponent(cc.Animation).play('electricityanim7');
        this.scheduleOnce(function () { _this.position3.destroy(); }, 0.8);
        this.position4.getComponent(cc.PhysicsBoxCollider).enabled = true;
        if (Math.random() > 0.5)
            this.position4.skewX = Math.random() * 30;
        else
            this.position4.skewX = Math.random() * -30;
        if (Math.random() > 0.5)
            this.position4.getComponent(cc.Animation).play('electricityanim1');
        else
            this.position4.getComponent(cc.Animation).play('electricityanim7');
        this.scheduleOnce(function () { _this.position4.destroy(); }, 0.8);
    };
    __decorate([
        property(cc.Node)
    ], Thirt_attack.prototype, "position0", void 0);
    __decorate([
        property(cc.Node)
    ], Thirt_attack.prototype, "position1", void 0);
    __decorate([
        property(cc.Node)
    ], Thirt_attack.prototype, "position2", void 0);
    __decorate([
        property(cc.Node)
    ], Thirt_attack.prototype, "position3", void 0);
    __decorate([
        property(cc.Node)
    ], Thirt_attack.prototype, "position4", void 0);
    Thirt_attack = __decorate([
        ccclass
    ], Thirt_attack);
    return Thirt_attack;
}(cc.Component));
exports.default = Thirt_attack;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=third_attack.js.map
        