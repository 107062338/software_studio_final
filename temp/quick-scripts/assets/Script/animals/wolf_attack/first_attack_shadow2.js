(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/Script/animals/wolf_attack/first_attack_shadow2.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '2e4cakolP1NZY8N5PoWnRCh', 'first_attack_shadow2', __filename);
// Script/animals/wolf_attack/first_attack_shadow2.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Shadow2 = /** @class */ (function (_super) {
    __extends(Shadow2, _super);
    function Shadow2() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    Shadow2.prototype.start = function () {
        var _this = this;
        this.getComponent(cc.Animation).play('wolf_run');
        this.node.runAction(cc.sequence(cc.moveBy(0.5, -220, -100), cc.callFunc(function () { _this.node.angle = 0; }), cc.spawn(cc.moveBy(0.5, -200, 0), cc.fadeOut(0.5)), cc.callFunc(function () { _this.node.destroy(); })));
    };
    Shadow2 = __decorate([
        ccclass
    ], Shadow2);
    return Shadow2;
}(cc.Component));
exports.default = Shadow2;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=first_attack_shadow2.js.map
        