(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/Script/animals/wolf_attack/first_attack_shadow1.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '93aafuoXJ9Hob2TcllyxMZi', 'first_attack_shadow1', __filename);
// Script/animals/wolf_attack/first_attack_shadow1.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Shadow1 = /** @class */ (function (_super) {
    __extends(Shadow1, _super);
    function Shadow1() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Shadow1.prototype.start = function () {
        var _this = this;
        this.getComponent(cc.Animation).play('wolf_run');
        this.node.runAction(cc.sequence(cc.moveBy(0.5, -240, 0), cc.spawn(cc.moveBy(0.5, -200, 0), cc.fadeOut(0.5)), cc.callFunc(function () { _this.node.destroy(); _this.node.parent.destroy(); })));
    };
    Shadow1 = __decorate([
        ccclass
    ], Shadow1);
    return Shadow1;
}(cc.Component));
exports.default = Shadow1;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=first_attack_shadow1.js.map
        