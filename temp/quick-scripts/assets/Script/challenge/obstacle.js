(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/Script/challenge/obstacle.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, 'cb6a9+IbudJgqix4e3QnMAR', 'obstacle', __filename);
// Script/challenge/obstacle.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var NewClass = /** @class */ (function (_super) {
    __extends(NewClass, _super);
    function NewClass() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.Anim = null;
        _this.Player = null;
        _this.cracking_sound = null;
        _this.contactCount = 0;
        _this.isContactFinished = true;
        return _this;
    }
    NewClass.prototype.start = function () {
        cc.director.getPhysicsManager().enabled = true;
        this.Anim = this.getComponent(cc.Animation);
    };
    NewClass.prototype.onBeginContact = function (contact, self, other) {
        var contactTag = other.tag;
        var player_speed_x = other.getComponent(cc.RigidBody).linearVelocity.x;
        var player_speed_y = other.getComponent(cc.RigidBody).linearVelocity.y;
        var player_velocity = Math.sqrt(player_speed_x * player_speed_x + player_speed_y * player_speed_y);
        if (contactTag == 0 && this.isContactFinished == true) {
            cc.log("player_velocity: " + player_velocity);
            switch (this.contactCount) {
                case 0:
                    cc.log("player 1");
                    this.Anim.play("collide_1");
                    cc.audioEngine.playEffect(this.cracking_sound, false);
                    break;
                case 1:
                    cc.log("player 2");
                    this.Anim.play("collide_2");
                    cc.audioEngine.playEffect(this.cracking_sound, false);
                    break;
                case 2:
                    cc.log("player 3");
                    this.Anim.play("collide_3");
                    cc.audioEngine.playEffect(this.cracking_sound, false);
                    this.schedule(this.FadeOut, 0.3);
                    break;
            }
            this.Player.getComponent(cc.RigidBody).linearVelocity = cc.v2(-200, 0);
            this.isContactFinished = false;
            this.contactCount += 1;
        }
    };
    NewClass.prototype.onEndContact = function (contact, self, other) {
        var contactTag = other.tag;
        if (contactTag == 0 && this.isContactFinished == false) {
            this.isContactFinished = true;
        }
    };
    NewClass.prototype.FadeOut = function () {
        var _this = this;
        var action = cc.fadeTo(1, 0);
        this.node.runAction(action);
        this.schedule(function () {
            _this.node.destroy();
        }, 1.0);
    };
    __decorate([
        property(cc.Node)
    ], NewClass.prototype, "Player", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], NewClass.prototype, "cracking_sound", void 0);
    NewClass = __decorate([
        ccclass
    ], NewClass);
    return NewClass;
}(cc.Component));
exports.default = NewClass;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=obstacle.js.map
        