(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/Script/challenge/attacking_sticks.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '7ed89RwikdLbY516BpVBKpY', 'attacking_sticks', __filename);
// Script/challenge/attacking_sticks.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var NewClass = /** @class */ (function (_super) {
    __extends(NewClass, _super);
    function NewClass() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.Player = null;
        _this.stick_1 = null;
        _this.stick_2 = null;
        _this.stick_3 = null;
        _this.stick_4 = null;
        _this.stick_5 = null;
        _this.stick_6 = null;
        _this.stick_7 = null;
        _this.stick_8 = null;
        _this.stick_9 = null;
        _this.stick_10 = null;
        _this.start_attacking = false;
        _this.droping_stick_index = 0;
        _this.is_attacking = [false, false, false, false, false, false, false, false, false, false];
        return _this;
    }
    NewClass.prototype.start = function () {
        cc.director.getPhysicsManager().enabled = true;
        this.stick_1.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
        this.stick_2.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
        this.stick_3.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
        this.stick_4.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
        this.stick_5.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
        this.stick_6.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
        this.stick_7.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
        this.stick_8.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
        this.stick_9.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
        this.stick_10.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
    };
    NewClass.prototype.onBeginContact = function (contact, self, other) {
        var contact_tag = other.tag;
        if (contact_tag == 0) {
            this.node.getComponent(cc.PhysicsBoxCollider).enabled = false;
            this.start_attacking = true;
        }
    };
    NewClass.prototype.update = function (dt) {
        var _this = this;
        if (!this.start_attacking) {
            return;
        }
        this.droping_stick_index += 1;
        var index = this.droping_stick_index / 15;
        cc.log(index);
        switch (index) {
            case 1:
                if (this.is_attacking[0]) {
                    return;
                }
                this.is_attacking[index - 1] = true;
                this.stick_1.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 2:
                if (this.is_attacking[1]) {
                    return;
                }
                this.is_attacking[index - 1] = true;
                this.stick_2.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 3:
                if (this.is_attacking[2]) {
                    return;
                }
                this.is_attacking[index - 1] = true;
                this.stick_3.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 4:
                if (this.is_attacking[3]) {
                    return;
                }
                this.is_attacking[index - 1] = true;
                this.stick_4.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 5:
                if (this.is_attacking[4]) {
                    return;
                }
                this.is_attacking[index - 1] = true;
                this.stick_5.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 6:
                if (this.is_attacking[5]) {
                    return;
                }
                this.is_attacking[index - 1] = true;
                this.stick_6.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 7:
                if (this.is_attacking[6]) {
                    return;
                }
                this.is_attacking[index - 1] = true;
                this.stick_7.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 8:
                if (this.is_attacking[7]) {
                    return;
                }
                this.is_attacking[index - 1] = true;
                this.stick_8.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 9:
                if (this.is_attacking[8]) {
                    return;
                }
                this.is_attacking[index - 1] = true;
                this.stick_9.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 10:
                if (this.is_attacking[9]) {
                    return;
                }
                this.is_attacking[index - 1] = true;
                this.stick_10.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 11:
                this.scheduleOnce(function () {
                    _this.node.destroy();
                }, 6.0);
                break;
        }
    };
    __decorate([
        property(cc.Node)
    ], NewClass.prototype, "Player", void 0);
    __decorate([
        property(cc.Node)
    ], NewClass.prototype, "stick_1", void 0);
    __decorate([
        property(cc.Node)
    ], NewClass.prototype, "stick_2", void 0);
    __decorate([
        property(cc.Node)
    ], NewClass.prototype, "stick_3", void 0);
    __decorate([
        property(cc.Node)
    ], NewClass.prototype, "stick_4", void 0);
    __decorate([
        property(cc.Node)
    ], NewClass.prototype, "stick_5", void 0);
    __decorate([
        property(cc.Node)
    ], NewClass.prototype, "stick_6", void 0);
    __decorate([
        property(cc.Node)
    ], NewClass.prototype, "stick_7", void 0);
    __decorate([
        property(cc.Node)
    ], NewClass.prototype, "stick_8", void 0);
    __decorate([
        property(cc.Node)
    ], NewClass.prototype, "stick_9", void 0);
    __decorate([
        property(cc.Node)
    ], NewClass.prototype, "stick_10", void 0);
    NewClass = __decorate([
        ccclass
    ], NewClass);
    return NewClass;
}(cc.Component));
exports.default = NewClass;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=attacking_sticks.js.map
        