(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/Script/challenge/lazer.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '185c8as2cdBtaDMj8YuEGOk', 'lazer', __filename);
// Script/challenge/lazer.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var NewClass = /** @class */ (function (_super) {
    __extends(NewClass, _super);
    function NewClass() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.lazerPrefab = null;
        _this.Player = null;
        _this.lazerAudio = null;
        _this.rotation_angle = 0;
        _this.Anim = null;
        _this.isAttacking = false;
        _this.isPlayerComing = false;
        _this.current_effect = null;
        _this.current_schedule = null;
        return _this;
    }
    NewClass.prototype.start = function () {
        cc.director.getPhysicsManager().enabled = true;
        this.Anim = this.getComponent(cc.Animation);
    };
    NewClass.prototype.update = function () {
        if (this.isAttacking) {
            this.node.getComponent(cc.PhysicsPolygonCollider).enabled = true;
        }
        else {
            this.node.getComponent(cc.PhysicsPolygonCollider).enabled = false;
        }
        var dx = Math.abs(this.node.position.x - this.Player.x);
        var dy = Math.abs(this.node.position.y - this.Player.y);
        if (this.isPlayerComing == false) {
            if (dx <= 960 && dy <= 720) {
                this.isPlayerComing = true;
                this.current_schedule = this.schedule(this.startAttacking, 8);
            }
        }
        else {
            if (dx >= 960 || dy >= 720) {
                this.isPlayerComing = false;
            }
        }
    };
    NewClass.prototype.startAttacking = function () {
        var _this = this;
        if (this.isPlayerComing == false) {
            cc.audioEngine.stopEffect(this.current_effect);
            return;
        }
        this.Anim.play("lazer_attack");
        var lazer_base_rotation_angle = this.rotation_angle % 180;
        var lazer_prefab_rotation_angle = (lazer_base_rotation_angle >= 90) ? (lazer_base_rotation_angle - 90) : lazer_base_rotation_angle;
        var lazer_base_w = this.node.height * Math.cos(lazer_prefab_rotation_angle);
        var lazer_base_h = this.node.height * Math.sin(lazer_prefab_rotation_angle);
        var lazer_one_x = this.node.position.x + lazer_base_w / 14;
        var lazer_one_y = this.node.position.y + lazer_base_h / 9;
        var lazer_two_x = this.node.position.x - lazer_base_w / 14;
        var lazer_two_y = this.node.position.y - lazer_base_h / 9;
        // Create lazer prefab
        var lazer_1 = cc.instantiate(this.lazerPrefab);
        lazer_1.parent = this.node.parent;
        lazer_1.position = cc.v2(lazer_one_x, lazer_one_y);
        lazer_1.angle = this.rotation_angle;
        var lazer_2 = cc.instantiate(this.lazerPrefab);
        lazer_2.parent = this.node.parent;
        lazer_2.position = cc.v2(lazer_two_x, lazer_two_y);
        lazer_2.angle = this.rotation_angle;
        this.isAttacking = true;
        this.current_effect = cc.audioEngine.playEffect(this.lazerAudio, true);
        this.scheduleOnce(function () {
            _this.Anim.play("lazer_base_idle");
            lazer_1.destroy();
            lazer_2.destroy();
            _this.isAttacking = false;
            cc.audioEngine.stopEffect(_this.current_effect);
        }, 6);
    };
    NewClass.prototype.onBeginContact = function (contact, self, other) {
        var collideTag = other.tag;
        // let contactDirection = contact.getWorldManifold().normal;
        if (collideTag == 0 && this.isAttacking) {
            this.Player.getComponent(cc.RigidBody).linearVelocity = cc.v2(-200, 0);
        }
    };
    __decorate([
        property(cc.Prefab)
    ], NewClass.prototype, "lazerPrefab", void 0);
    __decorate([
        property(cc.Node)
    ], NewClass.prototype, "Player", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], NewClass.prototype, "lazerAudio", void 0);
    __decorate([
        property()
    ], NewClass.prototype, "rotation_angle", void 0);
    NewClass = __decorate([
        ccclass
    ], NewClass);
    return NewClass;
}(cc.Component));
exports.default = NewClass;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=lazer.js.map
        