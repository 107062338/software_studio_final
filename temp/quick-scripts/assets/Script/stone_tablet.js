(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/Script/stone_tablet.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, 'db8b9RoMzBFXLNFCaHZQXJ6', 'stone_tablet', __filename);
// Script/stone_tablet.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var stone_tablet = /** @class */ (function (_super) {
    __extends(stone_tablet, _super);
    function stone_tablet() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.deny_sound = null;
        _this.accept_sound = null;
        _this.open_sound = null;
        _this.Anim = null;
        _this.isCollide = false;
        _this.stone_action = null;
        return _this;
    }
    stone_tablet.prototype.start = function () {
        this.Anim = this.getComponent(cc.Animation);
    };
    stone_tablet.prototype.update = function (dt) {
        cc.log(this.node.position);
    };
    stone_tablet.prototype.deny = function () {
        var _this = this;
        cc.audioEngine.playEffect(this.deny_sound, false);
        this.Anim.play("stone_tablet_deny");
        this.scheduleOnce(function () {
            _this.Anim.play("stone_tablet_idle");
        }, 3.0);
    };
    stone_tablet.prototype.accept = function () {
        var _this = this;
        if (this.isCollide) {
            return;
        }
        cc.audioEngine.playEffect(this.accept_sound, false);
        this.Anim.play("stone_tablet_accept");
        this.isCollide = true;
        this.scheduleOnce(function () {
            var stone_tablet_action = cc.moveTo(15, cc.v2(3810, -1000));
            _this.stone_action = _this.node.runAction(stone_tablet_action);
            cc.audioEngine.playEffect(_this.open_sound, false);
            _this.scheduleOnce(function () {
                _this.Anim.play("stone_tablet_idle");
            }, 15);
        }, 3.0);
    };
    stone_tablet.prototype.stopAction = function () {
        this.node.stopAction(this.stone_action);
    };
    stone_tablet.prototype.onBeginContact = function (contact, self, other) {
        var contact_tag = other.tag;
        if (contact_tag == 0) {
            var player_flower_count = other.getComponent("PlayerController").Flower;
            cc.log("player_flower_count is " + player_flower_count);
            if (player_flower_count == 3) {
                this.accept();
            }
            else {
                this.deny();
            }
        }
    };
    __decorate([
        property({ type: cc.AudioClip })
    ], stone_tablet.prototype, "deny_sound", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], stone_tablet.prototype, "accept_sound", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], stone_tablet.prototype, "open_sound", void 0);
    stone_tablet = __decorate([
        ccclass
    ], stone_tablet);
    return stone_tablet;
}(cc.Component));
exports.default = stone_tablet;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=stone_tablet.js.map
        