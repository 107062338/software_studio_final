(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/Script/flowers/flower_1.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '8d51fZ0Q/5CFJ6D9MXosb1a', 'flower_1', __filename);
// Script/flowers/flower_1.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var NewClass = /** @class */ (function (_super) {
    __extends(NewClass, _super);
    function NewClass() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.flower_sound_effect = null;
        _this.flower_animation_prefab = null;
        _this.Player = null;
        _this.playAnimation = false;
        return _this;
    }
    // public Anim: cc.Animation = null;
    NewClass.prototype.start = function () {
        // this.Anim = this.getComponent(cc.Animation);
    };
    NewClass.prototype.onBeginContact = function (contact, self, other) {
        if (other.tag == 0) {
            //this.node.getComponent(cc.PolygonCollider).enabled = false;
            this.playAnimation = true;
            // this.Anim.play("flower_1");
            cc.audioEngine.playEffect(this.flower_sound_effect, false);
            var action;
            action = cc.fadeTo(6, 0);
            this.node.runAction(action);
            this.schedule(this.fadeOut, 6);
        }
    };
    NewClass.prototype.add_life_animation = function () {
        var new_prefab = cc.instantiate(this.flower_animation_prefab);
        new_prefab.parent = this.node.parent.parent.parent.parent;
        new_prefab.position = this.node.position.sub(this.node.parent.parent.parent.parent.position);
        cc.log(this.node.parent.parent.parent.parent.position);
        cc.log(this.node.position);
        cc.log(this.Player.position);
        var action_move = cc.moveTo(1, this.Player.position);
        var action_fade = cc.fadeTo(0.5, 0);
        var animation_action = cc.sequence(action_move, action_fade);
        new_prefab.runAction(animation_action);
    };
    NewClass.prototype.fadeOut = function () {
        this.add_life_animation();
        this.node.destroy();
    };
    NewClass.prototype.PlayFinished = function () {
        if (this.playAnimation) { //} && !this.Anim.getAnimationState("flower_1").isPlaying) {
            return true;
        }
        else {
            return false;
        }
    };
    __decorate([
        property({ type: cc.AudioClip })
    ], NewClass.prototype, "flower_sound_effect", void 0);
    __decorate([
        property(cc.Prefab)
    ], NewClass.prototype, "flower_animation_prefab", void 0);
    __decorate([
        property(cc.Node)
    ], NewClass.prototype, "Player", void 0);
    NewClass = __decorate([
        ccclass
    ], NewClass);
    return NewClass;
}(cc.Component));
exports.default = NewClass;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=flower_1.js.map
        