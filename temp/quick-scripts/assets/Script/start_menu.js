(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/Script/start_menu.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, 'd52a1rjS9tC+acgXOwFYEnI', 'start_menu', __filename);
// Script/start_menu.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var NewClass = /** @class */ (function (_super) {
    __extends(NewClass, _super);
    function NewClass() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.start_menu_bgm = null;
        _this.enter_sound = null;
        _this.fog_1 = null;
        _this.fog_2 = null;
        _this.game_title = null;
        _this.start_btn = null;
        _this.is_start_enable = false;
        return _this;
    }
    NewClass.prototype.start = function () {
        var _this = this;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyDown, this);
        cc.audioEngine.playMusic(this.start_menu_bgm, true);
        this.title_animation();
        this.scheduleOnce(function () {
            _this.fog_animation();
            cc.director.preloadScene("map", function () {
                cc.log("map scene is loaded!");
            });
        }, 3);
    };
    NewClass.prototype.onKeyDown = function (event) {
        if (!this.is_start_enable) {
            return;
        }
        if (event.keyCode == cc.macro.KEY.enter) {
            cc.audioEngine.playEffect(this.enter_sound, false);
            this.schedule(function () {
                cc.audioEngine.stop(0);
                cc.director.loadScene("map");
            }, 2);
        }
    };
    NewClass.prototype.fog_animation = function () {
        var fog_1_action_r = cc.moveTo(9, cc.v2(750, 115));
        var fog_1_action_l = cc.moveTo(8, cc.v2(350, 115));
        var fog_1_action = cc.repeatForever(cc.sequence(fog_1_action_r, fog_1_action_l));
        this.fog_1.runAction(fog_1_action);
        var fog_2_action_r = cc.moveTo(7, cc.v2(650, 322));
        var fog_2_action_l = cc.moveTo(10, cc.v2(310, 322));
        var fog_2_action = cc.repeatForever(cc.sequence(fog_2_action_r, fog_2_action_l));
        this.fog_2.runAction(fog_2_action);
    };
    NewClass.prototype.title_animation = function () {
        var _this = this;
        var title_action = cc.moveTo(4, cc.v2(480, 540));
        this.game_title.runAction(title_action);
        this.schedule(function () {
            _this.start_btn.opacity = 255;
            var start_btn_action_fadein = cc.fadeIn(1.5);
            var start_btn_action_fadeout = cc.fadeOut(1.5);
            var start_btn_action = cc.repeatForever(cc.sequence(start_btn_action_fadein, start_btn_action_fadeout));
            _this.start_btn.runAction(start_btn_action);
            _this.is_start_enable = true;
        }, 5);
    };
    __decorate([
        property({ type: cc.AudioClip })
    ], NewClass.prototype, "start_menu_bgm", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], NewClass.prototype, "enter_sound", void 0);
    __decorate([
        property(cc.Node)
    ], NewClass.prototype, "fog_1", void 0);
    __decorate([
        property(cc.Node)
    ], NewClass.prototype, "fog_2", void 0);
    __decorate([
        property(cc.Node)
    ], NewClass.prototype, "game_title", void 0);
    __decorate([
        property(cc.Node)
    ], NewClass.prototype, "start_btn", void 0);
    NewClass = __decorate([
        ccclass
    ], NewClass);
    return NewClass;
}(cc.Component));
exports.default = NewClass;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=start_menu.js.map
        