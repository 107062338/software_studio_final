// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import { release } from "os";
import { runInThisContext } from "vm";

const { ccclass, property } = cc._decorator;

@ccclass
export default class PlayerController extends cc.Component {

    Jump: boolean = false;
    SpaceRelease: boolean = true;
    Left: boolean = false;
    Right: boolean = false;
    MoveSpeed: number = 0;
    JumpMode: number = 0; // 0: onGround 1: SingleJump 2: DoubleJump
    RigidBody: cc.RigidBody = null;
    public Anim: cc.Animation = null;
    AnimateState: cc.AnimationState = null;
    SoundController: any = null;
    StatusSystem: any = null;

    Charge: any = 0;
    isCharging: boolean = false;
    ChargeStartPos: cc.Vec2 = null;
    NextDirection: cc.Vec2 = null;
    Arrow: cc.Node = null;
    ChargeParticle: cc.Node = null;
    public isAttacking: number = 0;
    GameManagerSystem: any = null;
    inSkill: boolean = false;
    RepelForce: boolean = false;
    Skill2Timer: any = 0;
    isReturning: boolean = false;
    public Flower: number = 0;

    @property(cc.Node)
    background: cc.Node = null;

    @property(cc.Node)
    GameManager: cc.Node = null;

    @property(cc.Prefab)
    ArrowPrefab: cc.Prefab = null;

    @property(cc.Prefab)
    ChargePrefab: cc.Prefab = null;

    @property(cc.Prefab)
    ReleasePrefab: cc.Prefab = null;

    @property(cc.Prefab)
    public DamagePrefab: cc.Prefab = null;

    @property(cc.Prefab)
    ChargeOrbPrefab: cc.Prefab = null;

    @property(cc.Node)
    ChargeBar: cc.Node = null;

    ChargeOrb: cc.Node = null;

    @property(cc.Prefab)
    AttackPrefab: cc.Prefab = null;

    @property(cc.Prefab)
    Skill1: cc.Prefab = null;

    @property(cc.Prefab)
    Skill2_1: cc.Prefab = null;

    @property(cc.Prefab)
    Skill2_2: cc.Prefab = null;

    @property(cc.Prefab)
    Skill2_3: cc.Prefab = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }

    start() {
        this.RigidBody = this.getComponent(cc.RigidBody);
        this.Anim = this.node.getChildByName("Sprite").getComponent(cc.Animation);
        this.Anim.play("PlayerIdle");
        this.AnimateState = null;
        this.SoundController = this.getComponent("PlayerSoundController");
        this.StatusSystem = this.getComponent("PlayerStatusSystem");
        this.GameManagerSystem = this.GameManager.getComponent("GameManager");
        this.background.on(cc.Node.EventType.MOUSE_DOWN, this.onMouseDown, this);
        this.background.on(cc.Node.EventType.MOUSE_UP, this.onMouseUp, this);
        this.background.on(cc.Node.EventType.MOUSE_MOVE, this.onMouseMove, this);
    }

    update(dt) {
        if(this.GameManagerSystem.GameMode==0) {
            if(!this.StatusSystem.isDead) {
                if (!this.Anim.getAnimationState("PlayerSkill1").isPlaying && !this.Anim.getAnimationState("PlayerSkill2").isPlaying) {
                    this.inSkill = false;
                    this.RigidBody.gravityScale = 1;
                }

                if(!this.inSkill) {
                    if (this.isCharging) {
                        this.chargeEnergy(dt);
                    }
                    else {
                        this.playerMovement(dt);
                    }
                }
                else {
                    if(this.isAttacking == 3) {
                        this.Skill2Timer += dt;
                        if(this.Skill2Timer <= 0.35) {
                            if(this.node.getChildByName("Sprite").scaleX == 1) {
                                this.node.x += 2000 * dt;
                            }
                            else {
                                this.node.x -= 2000 * dt;
                            }
                        }
                        else {
                            if(!this.isReturning) {
                                this.isReturning = true;
                                this.node.getChildByName("Sprite").scaleX *= -1;
                            }
                            if(this.node.getChildByName("Sprite").scaleX == 1) {
                                this.node.x += 2000 * dt;
                            }
                            else {
                                this.node.x -= 2000 * dt;
                            }
                        }
                    }
                }
            }
            else {
                if(this.AnimateState==null || this.AnimateState.name != "PlayerDie") {
                    this.AnimateState = this.Anim.play("PlayerDie");
                    this.SoundController.PlaySoundEffect("Die");
                }
                this.GameManagerSystem.GameOver();
            }
        }
    }

    playerMovement(dt) {
        if (this.isAttacking == 1 || this.isAttacking == 2 || this.isAttacking == 3) {
            if(this.isAttacking == 1) {
                if (this.JumpMode == 0 && (this.AnimateState == null || (this.AnimateState.name != "PlayerAttack" && this.AnimateState.name != "PlayerAirAttack" && this.AnimateState.name != "PlayerSkill1" && this.AnimateState.name != "PlayerSkill2"))) {
                    this.AnimateState = this.Anim.play("PlayerAttack");
                    this.SoundController.PlaySoundEffect("Attack");
                    if(this.node.getChildByName("Sprite").scaleX == 1) {
                        this.node.x += 1500 * dt;
                    }
                    else {
                        this.node.x += -1500 * dt;
                    }
                }
                else if ((this.JumpMode == 1 || this.JumpMode == 2) && (this.AnimateState == null || (this.AnimateState.name != "PlayerAttack" && this.AnimateState.name != "PlayerAirAttack" && this.AnimateState.name != "PlayerSkill1" && this.AnimateState.name != "PlayerSkill2"))) {
                    this.AnimateState = this.Anim.play("PlayerAirAttack");
                    this.SoundController.PlaySoundEffect("Attack");
                    if(this.node.getChildByName("Sprite").scaleX == 1) {
                        this.node.x += 1500 * dt;
                    }
                    else {
                        this.node.x += -1500 * dt;
                    }
                }
            }
            else if(this.isAttacking == 2) {
                if(this.AnimateState == null || (this.AnimateState.name != "PlayerAttack" && this.AnimateState.name != "PlayerAirAttack" && this.AnimateState.name != "PlayerSkill1" && this.AnimateState.name != "PlayerSkill2")) {
                    this.AnimateState = this.Anim.play("PlayerSkill1");
                    this.SoundController.PlaySoundEffect("Skill1");
                    var senbonzakura_kageyoshi = cc.instantiate(this.Skill1);
                    senbonzakura_kageyoshi.parent = this.node.parent;
                    if(this.node.getChildByName("Sprite").scaleX == 1) {
                        senbonzakura_kageyoshi.position = this.node.position.sub(cc.v2(-50, 45));
                    }
                    else {
                        senbonzakura_kageyoshi.position = this.node.position.sub(cc.v2(50, 45));
                    }
                    senbonzakura_kageyoshi.getComponent("Skill1Controller").Direction = this.node.getChildByName("Sprite").scaleX;
                    this.inSkill = true;
                    this.RigidBody.gravityScale = 0;
                    this.RigidBody.linearVelocity = cc.Vec2.ZERO;
                    this.RigidBody.angularVelocity = 0;
                }
            }
            else if(this.isAttacking == 3) {
                if(this.AnimateState == null || (this.AnimateState.name != "PlayerAttack" && this.AnimateState.name != "PlayerAirAttack" && this.AnimateState.name != "PlayerSkill1" && this.AnimateState.name != "PlayerSkill2")) {
                    this.isReturning = false;
                    this.AnimateState = this.Anim.play("PlayerSkill2");
                    this.SoundController.PlaySoundEffect("Skill2");
                    var ichi_no_kata_list = [];
                    var dx = 185 / 2;
                    if(this.node.getChildByName("Sprite").scaleX == 1) {
                        for(var i = 0; i < 8; i++) {
                            var random = Math.ceil(Math.random() * 3);
                            var ichi_no_kata = null;
                            if(random == 1) {
                                ichi_no_kata = cc.instantiate(this.Skill2_1);
                            }
                            else if(random == 2) {
                                ichi_no_kata = cc.instantiate(this.Skill2_2);
                            }
                            else {
                                ichi_no_kata = cc.instantiate(this.Skill2_3);
                            }
                            ichi_no_kata.parent = this.node.parent;
                            ichi_no_kata.position = this.node.position.add(cc.v2(i * dx, 0));
                            ichi_no_kata_list.push(ichi_no_kata);
                        }
                    }
                    else {
                        for(var i = 0; i < 8; i++) {
                            var random = Math.ceil(Math.random() * 3);
                            var ichi_no_kata = null;
                            if(random == 1) {
                                ichi_no_kata = cc.instantiate(this.Skill2_1);
                            }
                            else if(random == 2) {
                                ichi_no_kata = cc.instantiate(this.Skill2_2);
                            }
                            else {
                                ichi_no_kata = cc.instantiate(this.Skill2_3);
                            }
                            ichi_no_kata.parent = this.node.parent;
                            ichi_no_kata.position = this.node.position.sub(cc.v2(i * dx, 0));
                            ichi_no_kata_list.push(ichi_no_kata);
                        }
                    }
                    this.scheduleOnce(()=> {
                        while(ichi_no_kata_list.length > 0) {
                            var ichi_no_kata: cc.Node = ichi_no_kata_list.pop();
                            ichi_no_kata.destroy();
                        }
                    }, 1.2); 
                    this.inSkill = true;
                    this.RigidBody.gravityScale = 0;
                    this.RigidBody.linearVelocity = cc.Vec2.ZERO;
                    this.RigidBody.angularVelocity = 0;
                }
            }

            if (!this.Anim.getAnimationState("PlayerAttack").isPlaying && !this.Anim.getAnimationState("PlayerAirAttack").isPlaying && !this.Anim.getAnimationState("PlayerSkill1").isPlaying && !this.Anim.getAnimationState("PlayerSkill2").isPlaying) {
                if(this.isAttacking == 2 || this.isAttacking == 3) {
                    this.AnimateState = this.Anim.play("PlayerFalling");
                }
                this.scheduleOnce(() => {
                    if(this.SoundController.IsCurrentPlaying("Skill2")) {
                        this.SoundController.StopCurrentSoundEffect();
                    }
                }, 1);
                this.Skill2Timer = 0;
                this.isAttacking = 0;
            }
        }

        this.MoveSpeed = 0;

        var isJumping = this.Jump && this.SpaceRelease;
        var isMoving = this.Left || this.Right;

        var AnimationPlaying = this.Anim.getAnimationState("PlayerJump").isPlaying || this.Anim.getAnimationState("PlayerDoubleJump").isPlaying || this.Anim.getAnimationState("PlayerAttack").isPlaying || this.Anim.getAnimationState("PlayerAirAttack").isPlaying || this.Anim.getAnimationState("PlayerSkill1").isPlaying || this.Anim.getAnimationState("PlayerSkill2").isPlaying;

        // animation
        if (!isJumping && !isMoving && this.isAttacking==0 && this.JumpMode == 0) {
            if (this.AnimateState != null && !AnimationPlaying) {
                this.Anim.play("PlayerIdle");
                this.AnimateState = null;
            }
        }

        if (isMoving && !this.SoundController.IsCurrentPlaying("Walking") && this.JumpMode == 0) {
            this.SoundController.PlaySoundEffect("Walking");
        }
        else if ((!isMoving || this.JumpMode != 0) && this.SoundController.IsCurrentPlaying("Walking")) {
            this.SoundController.StopCurrentSoundEffect();
        }

        // moving
        if (this.Left) {
            this.MoveSpeed = -300;
            this.node.getChildByName("Sprite").scaleX = -1;
            if(!this.RepelForce) {
                this.RigidBody.linearVelocity = cc.v2(this.RigidBody.linearVelocity.x/3, this.RigidBody.linearVelocity.y);
            }
            if ((this.AnimateState == null || this.AnimateState.name != "PlayerWalk") && !AnimationPlaying && this.JumpMode == 0) {
                this.AnimateState = this.Anim.play("PlayerWalk");
            }
        } else if (this.Right) {
            this.MoveSpeed = 300;
            this.node.getChildByName("Sprite").scaleX = 1;
            if(!this.RepelForce) {
                this.RigidBody.linearVelocity = cc.v2(this.RigidBody.linearVelocity.x/3, this.RigidBody.linearVelocity.y);
            }            
            if ((this.AnimateState == null || this.AnimateState.name != "PlayerWalk") && !AnimationPlaying && this.JumpMode == 0) {
                this.AnimateState = this.Anim.play("PlayerWalk");
            }
        }
        if(this.RepelForce) {
            this.MoveSpeed = 0;
        }
        this.node.x += this.MoveSpeed * dt;
        
        // jumping
        if (isJumping) {
            this.SpaceRelease = false;
            this.playerJump();
        }
    }

    playerJump() {
        if (this.JumpMode == 0 || this.JumpMode == 1) {
            this.RigidBody.applyForceToCenter(new cc.Vec2(0, 75000 * (this.JumpMode + 3)), true);
            this.JumpMode += 1;

            if (this.JumpMode == 1) {
                if(this.AnimateState==null || !this.Anim.getAnimationState("PlayerAttack").isPlaying && !this.Anim.getAnimationState("PlayerAirAttack").isPlaying && !this.Anim.getAnimationState("PlayerSkill1").isPlaying && !this.Anim.getAnimationState("PlayerSkill2").isPlaying) {
                    this.AnimateState = this.Anim.play("PlayerJump");
                }
                this.SoundController.PlaySoundEffect("Jump");
            }
            else {
                if(this.AnimateState==null || !this.Anim.getAnimationState("PlayerAttack").isPlaying && !this.Anim.getAnimationState("PlayerAirAttack").isPlaying && !this.Anim.getAnimationState("PlayerSkill1").isPlaying && !this.Anim.getAnimationState("PlayerSkill2").isPlaying) {
                    this.AnimateState = this.Anim.play("PlayerDoubleJump");
                }
                this.SoundController.PlaySoundEffect("DoubleJump");
            }
        }
    }

    onKeyDown(event) {
        if(this.GameManagerSystem.GameMode==1 || this.StatusSystem.isDead || this.inSkill) {
            return;
        }

        if (event.keyCode == cc.macro.KEY.space) {
            this.Jump = true;
        }
        else if (event.keyCode == cc.macro.KEY.a) {
            this.Left = true;
        }
        else if (event.keyCode == cc.macro.KEY.d) {
            this.Right = true;
        }
        else if (event.keyCode == cc.macro.KEY.f) {
            this.isAttacking = 1;
        }
        else if (event.keyCode == cc.macro.KEY.g) {
            if(this.StatusSystem.CurrentEnergy > 0) {
                this.isAttacking = 2;
                this.StatusSystem.changeEnergy(-1);
            }
        }
        else if (event.keyCode == cc.macro.KEY.h) {
            if(this.StatusSystem.CurrentEnergy == this.StatusSystem.Energy) {
                this.isAttacking = 3;
                this.StatusSystem.changeEnergy(-this.StatusSystem.Energy);
            }
        }
        else if (event.keyCode == cc.macro.KEY.p) {
            this.Flower = 3;
        }
    }

    onKeyUp(event) {
        if (event.keyCode == cc.macro.KEY.space) {
            this.Jump = false;
            this.SpaceRelease = true;
        }
        else if (event.keyCode == cc.macro.KEY.a) {
            this.Left = false;
        }
        else if (event.keyCode == cc.macro.KEY.d) {
            this.Right = false;
        }
    }

    onMouseDown(event) {
        if(this.GameManagerSystem.GameMode==1 || this.StatusSystem.isDead || this.inSkill) {
            return;
        }
        
        if(this.StatusSystem.CurrentEnergy > 0) {
            if(this.JumpMode != 0) {
                this.StatusSystem.changeEnergy(-1);
                this.ChargeOrb = cc.instantiate(this.ChargeOrbPrefab);
                this.ChargeOrb.parent = this.ChargeBar;
                this.ChargeOrb.position = cc.Vec2.ZERO;
                if (!this.isCharging) {
                    this.ChargeStartPos = this.node.position;
                    this.isCharging = true;
                    this.Arrow = cc.instantiate(this.ArrowPrefab);
                    this.Arrow.parent = this.node.parent;
                    this.Arrow.position = this.node.position;
                }
            }
        }
        event.stopPropagation();
    }

    onMouseUp(event) {
        if(this.GameManagerSystem.GameMode==1 || this.StatusSystem.isDead || this.inSkill) {
            return;
        }

        this.isCharging = false;
        this.releaseEnergy();
        event.stopPropagation();
    }

    onMouseMove(event) {
        if(this.GameManagerSystem.GameMode==1 || this.StatusSystem.isDead || this.inSkill) {
            return;
        }
        if (this.isCharging) {
            var ChargeCurPos: cc.Vec2 = null;
            ChargeCurPos = this.node.parent.convertToNodeSpaceAR(event.getLocation()).add(this.node.position);
            //cc.log("x:" + this.ChargeStartPos.x + " y:" + this.ChargeStartPos.y + " " + ChargeCurPos.x + " " + ChargeCurPos.y);
            this.NextDirection = this.ChargeStartPos.sub(ChargeCurPos);
            var normalized = this.NextDirection.normalize();
            var angle = Math.atan(Math.abs(normalized.y) / Math.abs(normalized.x)) * 180 / Math.PI;
            if (normalized.x > 0 && normalized.y > 0) {
                angle -= 90;
            }
            else if (normalized.x > 0 && normalized.y < 0) {
                angle = -angle + 270;
            }
            else if (normalized.x < 0 && normalized.y > 0) {
                angle = -angle - 270;
            }
            else if (normalized.x < 0 && normalized.y < 0) {
                angle += 90;
            }
            this.Arrow.angle = angle;
        }
        event.stopPropagation();
    }

    onBeginContact(contact, self, other) {
        if(this.GameManagerSystem.GameMode==1 || this.StatusSystem.isDead) {
            return;
        }

        if(this.isAttacking==3 && !(Math.round(contact.getWorldManifold().normal.y) == -1)) {
            contact.disabled = true;
            return;
        }
        /*
            0:      player
            1:      ground
            2:      thorn
            3:      minion
            4:      boss
            5:      wood
            6:      spike
            7:      laser
            8:      flower
            9:      door
            10:     portal
            11:     trigger
            1000:   melee 
            1001:   range
        */
        if (self.tag == 0) {
            if (other.tag == 1) {
                if (!(Math.round(contact.getWorldManifold().normal.y) == 1)) {
                    this.JumpMode = 0;
                }
            }
            else if (other.tag == 2) {
                this.StatusSystem.changeHP(-1);
                this.SoundController.PlaySoundEffect("Hurt");
                this.node.getChildByName("Sprite").color = cc.Color.RED;
                this.scheduleOnce(() => {
                    this.node.getChildByName("Sprite").color = cc.Color.WHITE;
                }, 0.5);
                this.JumpMode = 1;
                this.RepelForce = true;
                this.scheduleOnce(()=> {
                    this.RepelForce = false;
                }, 0.1);
                if(Math.round(contact.getWorldManifold().normal.x) == -1 && Math.round(contact.getWorldManifold().normal.y) == 0) {
                    this.RigidBody.linearVelocity = cc.v2(500, 0);
                }
                else if(Math.round(contact.getWorldManifold().normal.x) == 1 && Math.round(contact.getWorldManifold().normal.y) == 0) {
                    this.RigidBody.linearVelocity = cc.v2(-500, 0);                
                }
                else if(Math.round(contact.getWorldManifold().normal.x) == 0 && Math.round(contact.getWorldManifold().normal.y) == 1) {
                    this.RigidBody.linearVelocity = cc.v2(0, -500);
                }
                else if(Math.round(contact.getWorldManifold().normal.x) == 0 && Math.round(contact.getWorldManifold().normal.y) == -1) {
                    this.RigidBody.linearVelocity = cc.v2(0, 500);
                }
            }
            else if (other.tag == 3) {
                var behavior = "";
                if(other.node.name == "bear") {
                    behavior = other.node.getComponent("bear").behavior;
                }
                else if(other.node.name == "hawk") {
                    behavior = other.node.getComponent("hawk").behavior;

                }
                else if(other.node.name == "wild_boar") {
                    behavior = other.node.getComponent("wild_boar").behavior;

                }
                if (this.isAttacking==0 && behavior!="death") {
                    this.StatusSystem.changeHP(-1);
                    this.SoundController.PlaySoundEffect("Hurt");
                    this.node.getChildByName("Sprite").color = cc.Color.RED;
                    this.scheduleOnce(() => {
                        this.node.getChildByName("Sprite").color = cc.Color.WHITE;
                    }, 0.5);
                }
            }
            else if (other.tag == 4) {
                var behavior = "";
                if(other.node.name == "wolf") {
                    behavior = other.node.getComponent("wolf").behavior;
                }
                if (this.isAttacking==0 && behavior!="death") {
                    this.StatusSystem.changeHP(-2);
                    this.SoundController.PlaySoundEffect("Hurt");
                    this.node.getChildByName("Sprite").color = cc.Color.RED;
                    this.scheduleOnce(() => {
                        this.node.getChildByName("Sprite").color = cc.Color.WHITE;
                    }, 0.5);
                }
            }
            else if(other.tag == 5) {
                this.JumpMode = 1;
                this.RepelForce = true;
                this.scheduleOnce(()=> {
                    this.RepelForce = false;
                }, 0.1);
                if(Math.round(contact.getWorldManifold().normal.x) == -1 && Math.round(contact.getWorldManifold().normal.y) == 0) {
                    this.RigidBody.linearVelocity = cc.v2(500, 0);
                }
                else if(Math.round(contact.getWorldManifold().normal.x) == 1 && Math.round(contact.getWorldManifold().normal.y) == 0) {
                    this.RigidBody.linearVelocity = cc.v2(-500, 0);                
                }
                else if(Math.round(contact.getWorldManifold().normal.x) == 0 && Math.round(contact.getWorldManifold().normal.y) == 1) {
                    this.RigidBody.linearVelocity = cc.v2(0, -500);
                }
                else if(Math.round(contact.getWorldManifold().normal.x) == 0 && Math.round(contact.getWorldManifold().normal.y) == -1) {
                    this.RigidBody.linearVelocity = cc.v2(0, 500);
                }
            }
            else if(other.tag == 6) {
                if(Math.round(contact.getWorldManifold().normal.y) == -1) {
                    this.StatusSystem.changeHP(-1);
                    this.SoundController.PlaySoundEffect("Hurt");
                    this.node.getChildByName("Sprite").color = cc.Color.RED;
                    this.scheduleOnce(() => {
                        this.node.getChildByName("Sprite").color = cc.Color.WHITE;
                    }, 0.5);
                }
                this.JumpMode = 0;
            }
            else if(other.tag == 7) {
                this.StatusSystem.changeHP(-1);
                this.SoundController.PlaySoundEffect("Hurt");
                this.node.getChildByName("Sprite").color = cc.Color.RED;
                this.scheduleOnce(() => {
                    this.node.getChildByName("Sprite").color = cc.Color.WHITE;
                }, 0.5);
                this.JumpMode = 1;
                this.RepelForce = true;
                this.scheduleOnce(()=> {
                    this.RepelForce = false;
                }, 0.1);
                if(Math.round(contact.getWorldManifold().normal.x) == -1 && Math.round(contact.getWorldManifold().normal.y) == 0) {
                    this.RigidBody.linearVelocity = cc.v2(500, 0);
                }
                else if(Math.round(contact.getWorldManifold().normal.x) == 1 && Math.round(contact.getWorldManifold().normal.y) == 0) {
                    this.RigidBody.linearVelocity = cc.v2(-500, 0);                
                }
                else if(Math.round(contact.getWorldManifold().normal.x) == 0 && Math.round(contact.getWorldManifold().normal.y) == 1) {
                    this.RigidBody.linearVelocity = cc.v2(0, -500);
                }
                else if(Math.round(contact.getWorldManifold().normal.x) == 0 && Math.round(contact.getWorldManifold().normal.y) == -1) {
                    this.RigidBody.linearVelocity = cc.v2(0, 500);
                }
            }
            else if(other.tag == 8) {
                if(this.GameManagerSystem.GameMode == 0) {
                    this.Anim.play("PlayerIdle");
                    this.AnimateState = null;
                    this.GameManagerSystem.GameMode = 1;
                    this.scheduleOnce(() => {
                        this.StatusSystem.changeHP(1);
                    }, 2);
                    this.Flower += 1;
                    this.scheduleOnce(()=> {
                        this.GameManagerSystem.GameMode = 0;
                    }, 4)
                }
            }
            else if(other.tag == 11) {
                this.Anim.play("PlayerIdle");
                this.AnimateState = null;
                this.GameManagerSystem.BossTrigger();
                other.node.destroy();
            }
        }
        else if (self.tag == 1000) {
            if (other.tag == 3 || other.tag == 4) {
                if (this.isAttacking!=0) {
                    // instantiate particle effect
                    var DamageParticle = cc.instantiate(this.DamagePrefab);
                    DamageParticle.parent = other.node.parent;
                    DamageParticle.position = other.node.position;
                    this.scheduleOnce(() => {
                        DamageParticle.destroy();
                    }, 0.5);
                }
            }
        }
    }

    releaseEnergy() {
        if (this.NextDirection != null) {
            var normalized = this.NextDirection.normalizeSelf().mul(this.Charge * 250000);
            var finalDirection = cc.v2(Math.ceil(normalized.x), Math.ceil(normalized.y));
            this.RigidBody.applyForceToCenter(finalDirection, true);
            if (this.NextDirection.x > 0) {
                this.node.getChildByName("Sprite").scaleX = 1;
            }
            else {
                this.node.getChildByName("Sprite").scaleX = -1;
            }
            this.NextDirection = null;
            this.SoundController.StopCurrentSoundEffect();
            this.SoundController.PlaySoundEffect("Release");
        }
        if (this.Arrow != null) {
            var ReleaseParticle = cc.instantiate(this.ReleasePrefab);
            ReleaseParticle.parent = this.node.parent;
            ReleaseParticle.position = this.node.position;
            ReleaseParticle.angle = this.Arrow.angle;
            this.scheduleOnce(() => {
                ReleaseParticle.destroy();
            }, 0.5)
            this.Arrow.destroy();
            this.Arrow = null;
        }
        if (this.ChargeParticle != null) {
            this.ChargeParticle.destroy();
            this.ChargeParticle = null;
        }
        if (this.ChargeOrb != null) {
            this.ChargeOrb.destroy();
            this.ChargeOrb = null;
        }
        this.isCharging = false;
        this.RigidBody.gravityScale = 1;
        this.Charge = 0;
        this.Anim.stop();
        this.AnimateState = this.Anim.play("PlayerFalling");
    }

    chargeEnergy(dt) {
        if (this.AnimateState == null || this.AnimateState.name != "PlayerSpinning") {
            this.Anim.stop();
            this.AnimateState = this.Anim.play("PlayerSpinning");
            this.isAttacking = 0;
            this.SoundController.PlaySoundEffect("Charge");
            this.ChargeParticle = cc.instantiate(this.ChargePrefab);
            this.ChargeParticle.parent = this.node.parent;
            this.ChargeParticle.position = this.node.position;
        }

        this.Charge += dt;
        this.RigidBody.gravityScale = 0;
        this.RigidBody.linearVelocity = cc.Vec2.ZERO;
        this.RigidBody.angularVelocity = 0;
        if (this.Charge >= 2) {
            this.Charge = 0;
            this.releaseEnergy();
        }
        if(this.ChargeOrb != null) {
            this.ChargeOrb.scaleX = this.Charge;
            this.ChargeOrb.scaleY = this.Charge;
        }
    }
}
