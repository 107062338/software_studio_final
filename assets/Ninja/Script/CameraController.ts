// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import { isWorker } from "cluster";

const { ccclass, property } = cc._decorator;

@ccclass
export default class CameraController extends cc.Component {

    @property(cc.Node)
    player: cc.Node = null;

    @property(cc.Node)
    boss: cc.Node = null;

    @property(cc.Node)
    deer_male: cc.Node = null;

    GameManagerSystem: any = null;
    isMoving: boolean = false;
    isDeath: boolean = false;
    isFinished: boolean = false;
    public isTrigger: boolean = false;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {
        this.GameManagerSystem = this.getComponent("GameManager");
    }

    update(dt) {
        if (!this.isTrigger) {
            
            if (!this.isFinished) {
                this.node.x = this.player.x;
                this.node.y = this.player.y;
            }
            // detect boundary
            // todo
        }
        else {
            if(!this.isMoving) {
                this.isMoving = true;
                this.node.runAction(cc.moveTo(3, this.boss.x, this.boss.y));
            }
            this.scheduleOnce(()=>{ 
                this.boss.getComponent("wolf").state = "active";
            }, 3);
            this.scheduleOnce(()=> {
                this.isMoving = false;
                this.isTrigger = false;
                this.GameManagerSystem.GameMode = 0;
            }, 8);
        }

        if(!this.isFinished) {
            if(this.isDeath == false && this.boss.getComponent("wolf").behavior == "death"){
                this.isFinished = true;
                this.isDeath = true;
                
                this.boss.getComponent("wolf").play_death_animation();
                this.scheduleOnce(()=>{
                    this.node.runAction(cc.sequence(cc.moveTo(3, this.deer_male.x, this.deer_male.y), cc.callFunc(()=>{
                        this.deer_male.getComponent('deer_male').deer_male_anim();
                    })));
                }, 7);
                this.scheduleOnce(()=>{cc.director.loadScene('start');}, 22);
            }
        }
    }
}
