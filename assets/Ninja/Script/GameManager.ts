// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class GameManager extends cc.Component {

    @property(cc.Node)
    Player: cc.Node = null;

    @property(cc.Node)
    BlackScreen: cc.Node = null;

    @property({ type: cc.AudioClip })
    GameBgm: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    BossBgm: cc.AudioClip = null;

    PlayerStatusSystem: any = null;
    PlayerControllerSystem: any = null;
    CameraController: any = null;
    Timer: any = null;
    public GameMode: number = 0;
    /*
        0: GamePlay
        1: GamePause
    */

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getPhysicsManager().gravity = new cc.Vec2(0, -1000);
    }

    start () {
        this.PlayerStatusSystem = this.Player.getComponent("PlayerStatusSystem");
        this.PlayerControllerSystem = this.Player.getComponent("PlayerController");
        this.CameraController = this.getComponent("CameraController");
        this.BlackScreen.opacity = 0;
        this.BlackScreen.active = false;
        cc.audioEngine.playMusic(this.GameBgm, true);
    }

    update (dt) {
        if(this.PlayerStatusSystem.isDead) {
            this.Timer += dt;
        }
    }

    public GameOver() {
        //cc.audioEngine.stopAll();
        this.BlackScreen.active = true;
        this.scheduleOnce(() => {
            cc.director.loadScene("start");
        }, 5)

        if(this.Timer <= 5) {
            var alpha = Math.floor(this.Timer / 5 * 255);
            this.BlackScreen.opacity = alpha;
        }
    }

    public GameWin() {

    }

    public BossTrigger() {
        this.GameMode = 1;
        this.CameraController.isTrigger = true;
        this.PlayerControllerSystem.Flower = 0;
        cc.audioEngine.stopMusic();
        cc.audioEngine.playMusic(this.BossBgm, true);
    }
}
