// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    stone_tablet: cc.Node = null;

    start () {

    }

    onBeginContact(contact, self, other) {
        let contact_tag = other.tag;

        if (contact_tag == 0) {
            var stone_tablet_action = cc.moveTo(1, cc.v2(3810, 1764));
            this.stone_tablet.getComponent('stone_tablet').stopAction();
            this.stone_tablet.runAction(stone_tablet_action);
        }
    }
}
