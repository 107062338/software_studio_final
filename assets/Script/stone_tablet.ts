// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class stone_tablet extends cc.Component {

    @property({type: cc.AudioClip})
    deny_sound: cc.AudioClip = null;

    @property({type: cc.AudioClip})
    accept_sound: cc.AudioClip = null;

    @property({type: cc.AudioClip})
    open_sound: cc.AudioClip = null;

    Anim: cc.Animation = null;
    isCollide: boolean = false
    stone_action: any = null;

    start () {
        this.Anim = this.getComponent(cc.Animation);
    }
    
    update(dt) {
        cc.log(this.node.position);
    }

    deny() {
        cc.audioEngine.playEffect(this.deny_sound, false);
        this.Anim.play("stone_tablet_deny");

        this.scheduleOnce( () => {
            this.Anim.play("stone_tablet_idle");
        }, 3.0);
    }

    accept() {
        if (this.isCollide) { return; }

        cc.audioEngine.playEffect(this.accept_sound, false);
        this.Anim.play("stone_tablet_accept");
        this.isCollide = true;

        this.scheduleOnce( () => {
            var stone_tablet_action = cc.moveTo(15, cc.v2(3810, -1000));
            this.stone_action = this.node.runAction(stone_tablet_action);
            cc.audioEngine.playEffect(this.open_sound, false);
            
            this.scheduleOnce( () => {
                this.Anim.play("stone_tablet_idle");
            }, 15);
        }, 3.0);
    }

    stopAction() {
        this.node.stopAction(this.stone_action);
    }

    onBeginContact(contact, self, other) {
        let contact_tag = other.tag;

        if (contact_tag == 0) {
            let player_flower_count = other.getComponent("PlayerController").Flower;
            cc.log("player_flower_count is " + player_flower_count);

            if (player_flower_count == 3) {
                this.accept();
            } else {
                this.deny();
            }
        }
    }

}
