// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    Anim: cc.Animation = null;

    @property(cc.Node)
    Player: cc.Node = null;

    @property({type: cc.AudioClip})
    cracking_sound: cc.AudioClip = null;

    contactCount = 0;
    isContactFinished = true;

    start () {
        cc.director.getPhysicsManager().enabled = true;
        this.Anim = this.getComponent(cc.Animation);
    }

    onBeginContact(contact, self, other) {
        let contactTag = other.tag;
        let player_speed_x = other.getComponent(cc.RigidBody).linearVelocity.x;
        let player_speed_y = other.getComponent(cc.RigidBody).linearVelocity.y;
        let player_velocity = Math.sqrt(player_speed_x * player_speed_x + player_speed_y * player_speed_y);

        if (contactTag == 0 && this.isContactFinished == true) {

            cc.log("player_velocity: " + player_velocity);

            switch(this.contactCount) {
                case 0:
                    cc.log("player 1");
                    this.Anim.play("collide_1");
                    cc.audioEngine.playEffect(this.cracking_sound, false);
                    break;
                case 1:
                    cc.log("player 2");
                    this.Anim.play("collide_2");
                    cc.audioEngine.playEffect(this.cracking_sound, false);
                    break;
                case 2:
                    cc.log("player 3");
                    this.Anim.play("collide_3");
                    cc.audioEngine.playEffect(this.cracking_sound, false);
                    this.schedule(this.FadeOut, 0.3);
                    break;
            }

            this.Player.getComponent(cc.RigidBody).linearVelocity = cc.v2(-200, 0);
            this.isContactFinished = false;
            this.contactCount += 1;
        }
    }

    onEndContact(contact, self, other) {
        let contactTag = other.tag;

        if (contactTag == 0 && this.isContactFinished == false) {
            this.isContactFinished = true;
        }
    }

    FadeOut() {
        let action = cc.fadeTo(1, 0);
        this.node.runAction(action);

        this.schedule(() => {
            this.node.destroy();
        }, 1.0);
    }
}
