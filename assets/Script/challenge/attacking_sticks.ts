// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    Player: cc.Node = null;

    @property(cc.Node)
    stick_1: cc.Node = null;

    @property(cc.Node)
    stick_2: cc.Node = null;

    @property(cc.Node)
    stick_3: cc.Node = null;

    @property(cc.Node)
    stick_4: cc.Node = null;

    @property(cc.Node)
    stick_5: cc.Node = null;

    @property(cc.Node)
    stick_6: cc.Node = null;

    @property(cc.Node)
    stick_7: cc.Node = null;

    @property(cc.Node)
    stick_8: cc.Node = null;

    @property(cc.Node)
    stick_9: cc.Node = null;

    @property(cc.Node)
    stick_10: cc.Node = null;

    start_attacking = false;
    droping_stick_index = 0;

    is_attacking = [false, false, false, false, false, false, false, false, false, false];

    start () {
        cc.director.getPhysicsManager().enabled = true;
        this.stick_1.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
        this.stick_2.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
        this.stick_3.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
        this.stick_4.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
        this.stick_5.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
        this.stick_6.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
        this.stick_7.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
        this.stick_8.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
        this.stick_9.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
        this.stick_10.getComponent(cc.RigidBody).linearVelocity = cc.Vec2.ZERO;
    }

    onBeginContact(contact, self, other) {
        let contact_tag = other.tag;

        if(contact_tag == 0) {
            this.node.getComponent(cc.PhysicsBoxCollider).enabled = false;
            this.start_attacking = true;
        }
    }

    update (dt) {

        if (!this.start_attacking) {
            return;
        }

        this.droping_stick_index += 1;

        let index = this.droping_stick_index / 15;
        cc.log(index);

        switch(index) {
            case 1:
                if(this.is_attacking[0]) { return; }
                this.is_attacking[index - 1] = true;
                this.stick_1.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 2:
                if(this.is_attacking[1]) { return; }
                this.is_attacking[index - 1] = true;
                this.stick_2.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 3:
                if(this.is_attacking[2]) { return; }
                this.is_attacking[index - 1] = true;
                this.stick_3.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 4:
                if(this.is_attacking[3]) { return; }
                this.is_attacking[index - 1] = true;
                this.stick_4.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 5:
                if(this.is_attacking[4]) { return; }
                this.is_attacking[index - 1] = true;
                this.stick_5.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 6:
                if(this.is_attacking[5]) { return; }
                this.is_attacking[index - 1] = true;
                this.stick_6.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 7:
                if(this.is_attacking[6]) { return; }
                this.is_attacking[index - 1] = true;
                this.stick_7.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 8:
                if(this.is_attacking[7]) { return; }
                this.is_attacking[index - 1] = true;
                this.stick_8.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 9:
                if(this.is_attacking[8]) { return; }
                this.is_attacking[index - 1] = true;
                this.stick_9.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 10:
                if(this.is_attacking[9]) { return; }
                this.is_attacking[index - 1] = true;
                this.stick_10.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                break;
            case 11:
                this.scheduleOnce(() => {
                    this.node.destroy();
                }, 6.0);
                break;
        }
        
    }
}
