// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Prefab)
    lazerPrefab: cc.Prefab = null;

    @property(cc.Node)
    Player: cc.Node = null;

    @property({type: cc.AudioClip})
    lazerAudio: cc.AudioClip = null;

    @property()
    rotation_angle: number = 0;

    Anim: cc.Animation = null;

    isAttacking = false;
    isPlayerComing = false;

    current_effect: any = null;
    current_schedule: any = null;

    start () {
        cc.director.getPhysicsManager().enabled = true;
        this.Anim = this.getComponent(cc.Animation);
    }

    update() {
        if (this.isAttacking) {
            this.node.getComponent(cc.PhysicsPolygonCollider).enabled = true;
        } else {
            this.node.getComponent(cc.PhysicsPolygonCollider).enabled = false;
        }

            let dx = Math.abs(this.node.position.x - this.Player.x);
            let dy = Math.abs(this.node.position.y - this.Player.y);
        
        if (this.isPlayerComing == false) {
            if (dx <= 960 && dy <= 720) {
                this.isPlayerComing  = true;
                this.current_schedule = this.schedule(this.startAttacking, 8);
            }
        } else {
            if (dx >= 960 || dy >= 720) {
                this.isPlayerComing = false;
            }
        }
    }

    startAttacking() {
        if (this.isPlayerComing == false) {
            cc.audioEngine.stopEffect(this.current_effect);
            return;
        }

        this.Anim.play("lazer_attack");

        let lazer_base_rotation_angle = this.rotation_angle % 180;
        let lazer_prefab_rotation_angle = (lazer_base_rotation_angle >= 90) ? (lazer_base_rotation_angle - 90) : lazer_base_rotation_angle;
        let lazer_base_w = this.node.height * Math.cos(lazer_prefab_rotation_angle);
        let lazer_base_h = this.node.height * Math.sin(lazer_prefab_rotation_angle);

        let lazer_one_x = this.node.position.x + lazer_base_w / 14;
        let lazer_one_y = this.node.position.y + lazer_base_h / 9;
        let lazer_two_x = this.node.position.x - lazer_base_w / 14;
        let lazer_two_y = this.node.position.y - lazer_base_h / 9;

        // Create lazer prefab
        var lazer_1 = cc.instantiate(this.lazerPrefab);
        lazer_1.parent = this.node.parent;
        lazer_1.position = cc.v2(lazer_one_x, lazer_one_y);
        lazer_1.angle = this.rotation_angle;

        var lazer_2 = cc.instantiate(this.lazerPrefab);
        lazer_2.parent = this.node.parent;
        lazer_2.position = cc.v2(lazer_two_x, lazer_two_y);
        lazer_2.angle = this.rotation_angle;
        
        this.isAttacking = true;
        this.current_effect = cc.audioEngine.playEffect(this.lazerAudio, true);

        this.scheduleOnce(() => {
            this.Anim.play("lazer_base_idle");
            lazer_1.destroy();
            lazer_2.destroy();
            this.isAttacking = false;
            cc.audioEngine.stopEffect(this.current_effect);
        }, 6);
    }

    onBeginContact(contact, self, other) {
        let collideTag = other.tag;
        // let contactDirection = contact.getWorldManifold().normal;

        if (collideTag == 0 && this.isAttacking) {
            this.Player.getComponent(cc.RigidBody).linearVelocity = cc.v2(-200, 0);
        }
    }
}
