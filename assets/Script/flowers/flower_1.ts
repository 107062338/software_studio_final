// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property({type: cc.AudioClip})
    flower_sound_effect: cc.AudioClip = null;

    @property(cc.Prefab)
    flower_animation_prefab: cc.Prefab = null;

    @property(cc.Node)
    Player: cc.Node = null;

    playAnimation: boolean = false;
    // public Anim: cc.Animation = null;

    start () {
        // this.Anim = this.getComponent(cc.Animation);
    }

    onBeginContact(contact, self, other) {
        if(other.tag==0) {
            //this.node.getComponent(cc.PolygonCollider).enabled = false;
            this.playAnimation = true;
            // this.Anim.play("flower_1");
            cc.audioEngine.playEffect(this.flower_sound_effect, false);

            var action;
            action = cc.fadeTo(6, 0);
            this.node.runAction(action);
            this.schedule(this.fadeOut, 6);
        }
    }

    add_life_animation() {
        var new_prefab = cc.instantiate(this.flower_animation_prefab);
        new_prefab.parent = this.node.parent.parent.parent.parent;
        new_prefab.position = this.node.position.sub(this.node.parent.parent.parent.parent.position);

        cc.log(this.node.parent.parent.parent.parent.position);
        cc.log(this.node.position);
        cc.log(this.Player.position);

        var action_move = cc.moveTo(1, this.Player.position);
        var action_fade = cc.fadeTo(0.5, 0);
        var animation_action = cc.sequence(action_move, action_fade);

        new_prefab.runAction(animation_action);
    }

    fadeOut() {
        this.add_life_animation();
        this.node.destroy();
    }

    public PlayFinished() {
        if(this.playAnimation) { //} && !this.Anim.getAnimationState("flower_1").isPlaying) {
            return true;
        } else {
            return false;
        }
    }
}
