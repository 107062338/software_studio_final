// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Bat extends cc.Component {

    @property(cc.Node)
    Player: cc.Node = null;
    
    @property()
    distance: number = 400;

    @property()
    speed: number = 100;

    @property()
    width: number = 300;

    @property()
    height: number = 300;

    @property()
    x_offset: number = 0;

    @property()
    y_offset: number = 0;

    private state: string = 'stop';
    private anim: cc.Animation = null;
    private fly_action: cc.Action = null;
    private detect_left: number = 0;
    private detect_right: number = 0;
    private detect_top: number = 0;
    private detect_bottom: number = 0;
    start () {
        this.anim = this.getComponent(cc.Animation);
        this.fly_action = cc.repeatForever(cc.sequence(cc.callFunc(function(){ this.node.scaleX = -1;}, this), cc.moveBy(this.distance/this.speed, -this.distance, 0), cc.callFunc(function(){ this.node.scaleX = 1;}, this), cc.moveBy(this.distance*2/this.speed, this.distance*2, 0)));
        this.detect_left = this.node.x - this.width / 2 - this.x_offset;
        this.detect_right = this.node.x  + this.width / 2 - this.x_offset;
        this.detect_top = this.node.y + this.height / 2 + this.y_offset;
        this.detect_bottom = this.node.y - this.height / 2 + this.y_offset;
    }

    update () {
        if(this.state == 'stop'){
            if(this.Player.y < this.detect_top && this.Player.y > this.detect_bottom){
                if(this.Player.x < this.detect_right && this.Player.x > this.detect_left){
                    this.anim.play('bat_fly');
                    this.state = 'active';
                    this.node.runAction(this.fly_action);
                }
            }
        }
        if(this.state == 'active'){
            if(this.Player.y > this.detect_top || this.Player.y < this.detect_bottom || this.Player.x > this.detect_right || this.Player.x < this.detect_left){
                this.state = 'stop';
                this.anim.stop();
                this.node.stopAllActions();
            }
        }
    }
}
