// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Deer_male extends cc.Component {

    @property(cc.ParticleSystem)
    deer_male_particle: cc.ParticleSystem = null;

    private anim: cc.Animation = null;

    start () {
        this.node.runAction(cc.fadeOut(1));
        this.anim = this.getComponent(cc.Animation);
    }
    public deer_male_anim(){    //GameEngine calls this func after the camera focus on the deer_male
        this.node.runAction(cc.spawn(cc.callFunc(()=>{this.deer_male_particle.resetSystem(); this.anim.play('deer_final_male');}), cc.fadeIn(2), cc.moveBy(1.5, -150, 0)));
        this.scheduleOnce(()=>{this.node.runAction(cc.sequence(cc.spawn(cc.moveBy(2, 150, 0), cc.callFunc(()=>{this.node.scaleX = -1;}), cc.fadeOut(2)), cc.callFunc(()=>{this.node.destroy()})))}, 6);
    }
}
