// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Deer_female extends cc.Component {

    @property(cc.Node)
    Player: cc.Node = null;

    @property()
    limit: number = 400;
    
    @property()
    width: number = 300;

    @property()
    height: number = 300;

    @property()
    x_offset: number = 0;

    @property()
    y_offset: number = 0;

    private anim: cc.Animation = null;
    private state: string = 'stop';
    private behavior: string = '';
    private direction: number = 1;
    private distance: number = 0;
    private detect_left: number = 0;
    private detect_right: number = 0;
    private detect_top: number = 0;
    private detect_bottom: number = 0;

    start () {
        this.anim = this.getComponent(cc.Animation);
        this.detect_left = this.node.x - this.width / 2 - this.x_offset;
        this.detect_right = this.node.x  + this.width / 2 - this.x_offset;
        this.detect_top = this.node.y + this.height / 2 + this.y_offset;
        this.detect_bottom = this.node.y - this.height / 2 + this.y_offset;
    }

    update (dt) {
        if(this.state == 'stop'){
            if(this.Player.y < this.detect_top && this.Player.y > this.detect_bottom){
                if(this.Player.x < this.detect_right && this.Player.x > this.detect_left){
                    this.state = 'active';
                    this.idle_behavior();
                }
            }
        }
        if(this.state == 'active'){
            if(this.Player.y > this.detect_top || this.Player.y < this.detect_bottom || this.Player.x > this.detect_right || this.Player.x < this.detect_left){
                this.state = 'stop';
                this.behavior = '';
                this.unscheduleAllCallbacks();
                this.anim.stop();
            }
        }
        if(this.behavior == 'run'){
            this.node.x += this.direction;
            this.distance += 2 * this.direction;
        }
        else if(this.behavior == 'walk'){
            this.node.x += 0.5 *this.direction;
            this.distance += this.direction;
        }
        if(this.distance >= this.limit){
            this.unscheduleAllCallbacks();
            this.distance = this.limit - 1;
            this.idle_behavior();
        }
        else if(this.distance <= -this.limit){
            this.unscheduleAllCallbacks();
            this.distance = -this.limit + 1;
            this.idle_behavior();
        }
    }

    private idle_behavior(){
        let time = Math.random() * 1.5 + 3;
        if(this.behavior != 'idle'){
            this.anim.play('deer_idle_female');
            this.behavior = 'idle';
        }
        if(this.state != 'stop'){
            let random = Math.random();
            if(random > 0.67){
                this.scheduleOnce(this.run_behavior, time);
            }
            else if(random > 0.33){
                this.scheduleOnce(this.walk_behavior, time);
            }
            else{
                this.scheduleOnce(this.eat_behavior, time);
            }
        }
    }
    private run_behavior(){
        let time = ((Math.random() * 5) / 1.14) * 1.14 + 1.14;
        if(this.behavior != 'run'){
            this.anim.play('deer_run_female');
            this.behavior = 'run';
            if(this.distance >= this.limit - 1){
                this.direction = -1;
                this.node.scaleX = 1;
            }
            else if(this.distance <= -this.limit + 1){
                this.direction = 1;
                this.node.scaleX = -1;   
            }
            else{
                if(Math.random() > 0.5){
                    this.direction = -1;
                    this.node.scaleX = 1;
                }
                else{
                    this.direction = 1;
                    this.node.scaleX = -1;                
                }
            }
        }        
        if(this.state != 'stop'){
            let random = Math.random();
            if(random > 0.67){
                this.scheduleOnce(this.idle_behavior, time);
            }
            else if(random > 0.33){
                this.scheduleOnce(this.walk_behavior, time);
            }
            else{
                this.scheduleOnce(this.eat_behavior, time);
            }
        }
    }
    private walk_behavior(){
        let time = ((Math.random() * 5) / 0.94) * 0.94 + 0.94;
        if(this.behavior != 'walk'){
            this.anim.play('deer_walk_female');
            this.behavior = 'walk';
            if(this.distance >= this.limit - 1){
                this.direction = -1;
                this.node.scaleX = 1;
            }
            else if(this.distance <= -this.limit + 1){
                this.direction = 1;
                this.node.scaleX = -1;   
            }
            else{
                if(Math.random() > 0.5){
                    this.direction = -1;
                    this.node.scaleX = 1;
                }
                else{
                    this.direction = 1;
                    this.node.scaleX = -1;                
                }
            }
        }        
        if(this.state != 'stop'){
            let random = Math.random();
            if(random > 0.67){
                this.scheduleOnce(this.idle_behavior, time);
            }
            else if(random > 0.33){
                this.scheduleOnce(this.run_behavior, time);
            }
            else{
                this.scheduleOnce(this.eat_behavior, time);
            }
        }
    }
    private eat_behavior(){
        let time = Math.random() * 1.5 + 2.67;
        if(this.behavior != 'eat'){
            this.anim.play('deer_eat_female');
            this.behavior = 'eat';
        }        
        if(this.state != 'stop'){
            let random = Math.random();
            if(random > 0.67){
                this.scheduleOnce(this.idle_behavior, time);
            }
            else if(random > 0.33){
                this.scheduleOnce(this.walk_behavior, time);
            }
            else{
                this.scheduleOnce(this.run_behavior, time);
            }
        }
    }
    //private death_behavior(){}

    onBeginContact(contact, self, other) {
        if(other.node.name == 'rabbit' || other.node.name == 'Player' || other.node.name == 'bear' || other.node.name == 'wild_boar'){
            contact.disabled = true;
        }
    }
}
