// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Wolf extends cc.Component {

    @property(cc.Node)
    Player: cc.Node = null;
    
    @property(cc.ParticleSystem)
    wolf_pre_repair: cc.ParticleSystem = null;

    @property(cc.ParticleSystem)
    wolf_repair: cc.ParticleSystem = null;

    @property(cc.Prefab)
    first_attack_prefab: cc.Prefab = null;

    @property(cc.Prefab)
    second_attack_prefab: cc.Prefab = null;

    @property(cc.Prefab)
    third_attack_prefab: cc.Prefab = null;

    @property(cc.AudioClip)
    wolf_howl_sound: cc.AudioClip = null;

    @property(cc.AudioClip)
    repair_sound: cc.AudioClip = null;

    @property(cc.AudioClip)
    first_sound: cc.AudioClip = null;

    @property(cc.AudioClip)
    third_sound: cc.AudioClip = null;

    @property(cc.Prefab)
    target: cc.Prefab = null;

    @property(cc.ParticleSystem)
    wolf_first_attack_particle: cc.ParticleSystem = null;

    @property(cc.ParticleSystem)
    wolf_second_attack_particle: cc.ParticleSystem = null;

    @property(cc.ParticleSystem)
    wolf_third_attack_particle: cc.ParticleSystem = null;

    @property(cc.ParticleSystem)
    wolf_death_particle: cc.ParticleSystem = null;

    @property(cc.Node)
    wolf_hp: cc.Node = null;

    public state: string = 'stop'; // stop, active
    private behavior: string = 'start'; // begining, idle, walk, repair, preatk, first_attack, second_attack, third_attack, death
    private anim: cc.Animation = null;
    public health: number = 200;
    private death_walk: boolean = false;
    private direction: number = 1;

    start () {
        this.wolf_hp.active = false;
        this.anim = this.getComponent(cc.Animation);
    }

    update(dt){
        this.wolf_hp.getComponent(cc.Sprite).fillRange = this.health / 200;
        if(this.state != 'stop'){   //GameEngine set this.state = 'active';
            if(this.behavior == 'start'){
                this.behavior = 'begining';
                cc.audioEngine.playEffect(this.wolf_howl_sound, false);
                this.anim.play('wolf_begining'); // Call idle_behavior at the end
                this.scheduleOnce(this.idle_behavior, 6);
                this.scheduleOnce(()=> {
                    this.wolf_hp.active = true;
                }, 5);
            }
            else if(this.behavior == 'walk'){
                this.node.x += 2 * this.direction;
            }
        }
        if(this.health <= 0  && this.behavior != 'death'){
            this.health = 0;
            this.behavior = 'death';
            this.unscheduleAllCallbacks();
            //this.play_death_animation(); Gameengine calls this func after detecting that this.behavior == 'death' and the camera had focus on the wolf
        }
        if(this.death_walk == true){
            this.node.x += -this.node.scaleX;
        }
    }

    private idle_behavior(){
        if(this.behavior != 'idle' && this.behavior != 'death'){
            this.behavior = 'idle';
            this.anim.play('wolf_idle');
            this.scheduleOnce(()=>{
                let random = Math.random();
                if(random > 0.5) this.walk_behavior();  
                else if(random > 0.4) this.repair_behavior();
                else this.preatk_behavior();
            }, 2);
        }
    }

    private walk_behavior(){
        if(this.behavior != 'walk' && this.behavior != 'death'){
            this.behavior = 'walk';
            this.anim.play('wolf_walk');
            if(Math.random() > 0.5){
                this.direction = -1;
                this.node.scaleX = 1;
            }
            else{
                this.direction = 1;
                this.node.scaleX = -1;
            }
            this.scheduleOnce(()=>{
                let random = Math.random();
                if(random > 0.4) this.idle_behavior();  
                else if(random > 0.3) this.repair_behavior();
                else this.preatk_behavior();
            }, 1.55);
        }
    }

    private repair_behavior(){
        if(this.behavior != 'repair' && this.behavior != 'death'){
            this.behavior = 'repair';
            this.anim.play('wolf_howl');
            cc.audioEngine.playEffect(this.repair_sound, false);
            this.wolf_pre_repair.resetSystem();
            this.scheduleOnce(()=>{this.anim.play('wolf_howl');}, 1.9);
            this.scheduleOnce(()=>{this.anim.play('wolf_howl');}, 3.8);
            this.scheduleOnce(()=>{
                if(this.health < 100) this.health += 100;
                else this.health = 200;
                this.wolf_repair.resetSystem();                
                let random = Math.random();
                if(random > 0.75) this.idle_behavior();  
                else if(random > 0.3) this.walk_behavior();
                else this.preatk_behavior();
            }, 5.7);
        }
    }

    private preatk_behavior(){
        if(this.behavior != 'death'){
            this.behavior = 'preatk';
            this.anim.play('wolf_attack');
            let random = Math.random();
            if(random > 0.67){
                this.wolf_first_attack_particle.resetSystem();
                this.first_attack();
            }
            else if(random > 0.33){
                this.wolf_second_attack_particle.resetSystem();
                this.second_attack();
            }
            else{
                this.wolf_third_attack_particle.resetSystem();
                this.third_attack();
            }
            this.scheduleOnce(()=>{
                let random = Math.random();
                if(random > 0.9)   this.repair_behavior();
                else if(random > 0.6)  this.walk_behavior();
                else if(random > 0.1) this.idle_behavior();
                else this.preatk_behavior();
            }, 1.1);
        }
    }

    private first_attack(){
        cc.audioEngine.playEffect(this.first_sound, false);
        let tar = cc.instantiate(this.target);
        tar.x = this.Player.x;
        tar.y = this.Player.y;
        this.node.parent.addChild(cc.instantiate(tar));
        let attack = cc.instantiate(this.first_attack_prefab);
        attack.x = this.Player.x;
        attack.y = this.Player.y;
        this.scheduleOnce(()=>{
            this.node.parent.addChild(attack);
        }, 1.5);
    }
    
    private second_attack(){
        let tar = cc.instantiate(this.target);
        tar.x = this.Player.x;
        tar.y = this.Player.y;
        this.node.parent.addChild(cc.instantiate(tar));
        let attack = cc.instantiate(this.second_attack_prefab);
        attack.x = this.Player.x;
        attack.y = this.Player.y;
        this.scheduleOnce(()=>{
            this.node.parent.addChild(attack);
        }, 1.5);
    }

    private third_attack(){
        this.scheduleOnce(()=>{cc.audioEngine.playEffect(this.third_sound, false);}, 1);
        let tar = cc.instantiate(this.target);
        tar.x = this.Player.x;
        tar.y = this.Player.y;
        this.node.parent.addChild(cc.instantiate(tar));
        let attack = cc.instantiate(this.third_attack_prefab);
        attack.x = this.Player.x;
        attack.y = this.Player.y;
        this.scheduleOnce(()=>{
            this.node.parent.addChild(attack);
        }, 2);
    }

    public play_death_animation(){
        this.anim.play('wolf_death');
        this.scheduleOnce(()=>{this.wolf_death_particle.resetSystem();}, 3);
        this.scheduleOnce(()=>{this.death_walk == true;}, 6);
        this.scheduleOnce(()=>{this.node.runAction(cc.sequence(cc.fadeOut(3), cc.callFunc(()=>{this.node.destroy();})))}, 5);
    }

    onBeginContact(contact, self, other) {
        if(other.tag == 1001  && this.behavior != 'death'){
            contact.disabled = true;
            this.health -= Math.random() * 30 / 1;
            if(this.behavior == 'repair'){
                this.unscheduleAllCallbacks();
                this.wolf_pre_repair.stopSystem();
                this.idle_behavior();
            }
        }
        else if(other.tag == 1000 && this.behavior != 'death'){
            contact.disabled = true;
            if(other.node.getComponent('PlayerController').isAttacking != 0){
                this.health -= Math.random() * 30 / 1;
                if(this.behavior == 'repair'){
                    this.unscheduleAllCallbacks();
                    this.wolf_pre_repair.stopSystem();
                    this.idle_behavior();
                }
            }
        }
        else if(other.tag == 1002 && this.behavior != 'death'){
            contact.disabled = true;
            this.health -= 100;
            if(this.behavior == 'repair'){
                this.unscheduleAllCallbacks();
                this.wolf_pre_repair.stopSystem();
                this.idle_behavior();
            }
        }
        if(other.node.name == 'Player'){
            contact.disabled = true;
        }
    }
}
