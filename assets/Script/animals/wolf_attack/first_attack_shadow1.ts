// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Shadow1 extends cc.Component {

    start () {
        this.getComponent(cc.Animation).play('wolf_run');
        this.node.runAction(cc.sequence(cc.moveBy(0.5, -240, 0), cc.spawn(cc.moveBy(0.5, -200, 0), cc.fadeOut(0.5)), cc.callFunc(()=>{this.node.destroy(); this.node.parent.destroy();})));
    }
    // update (dt) {}
}
