// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Thirt_attack extends cc.Component {

    @property(cc.Node)
    position0: cc.Node = null;

    @property(cc.Node)
    position1: cc.Node = null;

    @property(cc.Node)
    position2: cc.Node = null;

    @property(cc.Node)
    position3: cc.Node = null;

    @property(cc.Node)
    position4: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

     onLoad () {

    }

    start () {
        this.position0.getComponent(cc.PhysicsBoxCollider).enabled = true;
        if(Math.random() > 0.5) this.position0.skewX = Math.random() * 30;
        else this.position0.skewX = Math.random() * -30;
        if(Math.random() > 0.5) this.position0.getComponent(cc.Animation).play('electricityanim1');
        else this.position0.getComponent(cc.Animation).play('electricityanim7');
        this.scheduleOnce(()=>{this.position0.destroy()}, 0.8);
        this.position1.getComponent(cc.PhysicsBoxCollider).enabled = true;
        if(Math.random() > 0.5) this.position1.skewX = Math.random() * 30;
        else this.position1.skewX = Math.random() * -30;
        if(Math.random() > 0.5) this.position1.getComponent(cc.Animation).play('electricityanim1');
        else this.position1.getComponent(cc.Animation).play('electricityanim7');
        this.scheduleOnce(()=>{this.position1.destroy()}, 0.8);
        this.position2.getComponent(cc.PhysicsBoxCollider).enabled = true;
        if(Math.random() > 0.5) this.position2.skewX = Math.random() * 30;
        else this.position2.skewX = Math.random() * -30;
        if(Math.random() > 0.5) this.position2.getComponent(cc.Animation).play('electricityanim1');
        else this.position2.getComponent(cc.Animation).play('electricityanim7');
        this.scheduleOnce(()=>{this.position2.destroy()}, 0.8);
        this.position3.getComponent(cc.PhysicsBoxCollider).enabled = true;
        if(Math.random() > 0.5) this.position3.skewX = Math.random() * 30;
        else this.position3.skewX = Math.random() * -30;
        if(Math.random() > 0.5) this.position3.getComponent(cc.Animation).play('electricityanim1');
        else this.position3.getComponent(cc.Animation).play('electricityanim7');
        this.scheduleOnce(()=>{this.position3.destroy()}, 0.8);
        this.position4.getComponent(cc.PhysicsBoxCollider).enabled = true;
        if(Math.random() > 0.5) this.position4.skewX = Math.random() * 30;
        else this.position4.skewX = Math.random() * -30;
        if(Math.random() > 0.5) this.position4.getComponent(cc.Animation).play('electricityanim1');
        else this.position4.getComponent(cc.Animation).play('electricityanim7');
        this.scheduleOnce(()=>{this.position4.destroy()}, 0.8);
    }

    // update (dt) {}
}
