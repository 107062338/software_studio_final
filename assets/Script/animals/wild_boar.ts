// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Wild_boar extends cc.Component {

    @property(cc.Node)
    Player: cc.Node = null;

    @property()
    limit: number = 400;

    @property(cc.ParticleSystem)
    death_particle: cc.ParticleSystem = null;

    @property(cc.ParticleSystem)
    attack_particle: cc.ParticleSystem = null;

    @property(cc.AudioClip)
    attack_sound: cc.AudioClip = null;

    @property(cc.AudioClip)
    death_sound: cc.AudioClip = null;

    @property()
    width: number = 300;

    @property()
    height: number = 300;

    @property()
    x_offset: number = 0;

    @property()
    y_offset: number = 0;

    private anim: cc.Animation = null;
    private state: string = 'stop';
    public behavior: string = '';
    private direction: number = 1;
    private distance: number = 0;
    private hunt: boolean = true;
    private life_bar: cc.Node;
    private hurted_enable: boolean = true;
    private health: number = 80;
    private detect_left: number = 0;
    private detect_right: number = 0;
    private detect_top: number = 0;
    private detect_bottom: number = 0;
    start () {
        this.anim = this.getComponent(cc.Animation);
        this.detect_left = this.node.x - this.width / 2 - this.x_offset;
        this.detect_right = this.node.x  + this.width / 2 - this.x_offset;
        this.detect_top = this.node.y + this.height / 2 + this.y_offset;
        this.detect_bottom = this.node.y - this.height / 2 + this.y_offset;
        this.life_bar = this.node.getChildByName('wild_boar_life');
    }

    update (dt) {
        if(this.node.scaleX == -1){ //right
            this.life_bar.x = (80 - this.health)/2;
            this.life_bar.width = this.health;
            this.life_bar.angle = this.node.angle;
        }
        else{   //left
            this.life_bar.x = -(80 - this.health)/2;
            this.life_bar.width = this.health;
            this.life_bar.angle = -this.node.angle;
        }
        if(this.state == 'stop'){
            if(this.Player.y < this.detect_top && this.Player.y > this.detect_bottom){
                if(this.Player.x < this.detect_right && this.Player.x > this.detect_left){
                    this.state = 'active';
                    this.idle_behavior();
                }
            }
        }
        if(this.state == 'active' && this.behavior != 'death'){
            if(this.Player.y > this.detect_top || this.Player.y < this.detect_bottom || this.Player.x > this.detect_right || this.Player.x < this.detect_left){
                this.state = 'stop';
                this.behavior = '';
                this.hunt = true;
                this.anim.stop();
                this.unscheduleAllCallbacks();
            }
        }
        if(Math.abs(this.Player.x - this.node.x) < 250 && Math.abs(this.Player.y - this.node.y) < 250 && this.state == 'active' && this.hunt == true && this.behavior != 'death'){
            this.unscheduleAllCallbacks();
            this.run_behavior();
        }
        if(this.behavior == 'run'){
            this.node.x += this.direction;
            this.distance += 2 * this.direction;
        }
        else if(this.behavior == 'walk'){
            this.node.x += 0.5 *this.direction;
            this.distance += this.direction;
        }
        if(this.distance >= this.limit && this.behavior != 'death'){
            this.unscheduleAllCallbacks();
            if(this.behavior == 'run')  this.attack_particle.stopSystem();
            this.distance = this.limit - 1;
            this.idle_behavior();
        }
        else if(this.distance <= -this.limit && this.behavior != 'death'){
            this.unscheduleAllCallbacks();
            if(this.behavior == 'run')  this.attack_particle.stopSystem();
            this.distance = -this.limit + 1;
            this.idle_behavior();
        }
    }

    private idle_behavior(){
        this.hurted_enable = true;
        let time = Math.random() + 1.88;
        if(this.behavior != 'idle' && this.behavior != 'death'){
            this.anim.play('wild_boar_idle');
            this.behavior = 'idle';
            this.scheduleOnce(()=>{this.hunt = true;}, 7);
        }
        if(this.state != 'stop'){
            let random = Math.random();
            if(random > 0.5){
                this.scheduleOnce(this.walk_behavior, time);
            }
            else{
                this.scheduleOnce(this.eat_behavior, time);
            }
        }
    }
    private run_behavior(){
        if(this.behavior != 'run' && this.behavior != 'death' && this.hunt == true){
            this.hurted_enable = true;
            this.anim.play('wild_boar_run');
            this.behavior = 'run';
            cc.audioEngine.playEffect(this.attack_sound, false);
            this.hunt = false;
            this.attack_particle.resetSystem();
            if(this.Player.x - this.node.x > 0){
                this.direction = 1;
                this.node.scaleX = -1; 
            }
            else{
                this.direction = -1;
                this.node.scaleX = 1;           
            }
        }        
    }
    private walk_behavior(){
        let time = ((Math.random() * 4) / 2.0) * 2.0 + 2.0;
        if(this.behavior != 'walk' && this.behavior != 'death'){
            this.anim.play('wild_boar_walk');
            this.behavior = 'walk';
            if(this.distance >= this.limit - 1){
                this.direction = -1;
                this.node.scaleX = 1;
            }
            else if(this.distance <= -this.limit + 1){
                this.direction = 1;
                this.node.scaleX = -1;   
            }
            else{
                if(Math.random() > 0.5){
                    this.direction = -1;
                    this.node.scaleX = 1;
                }
                else{
                    this.direction = 1;
                    this.node.scaleX = -1;                
                }
            }
        }        
        if(this.state != 'stop'){
            let random = Math.random();
            if(random > 0.5){
                this.scheduleOnce(this.idle_behavior, time);
            }
            else{
                this.scheduleOnce(this.eat_behavior, time);
            }
        }
    }
    private eat_behavior(){
        let time = Math.random() + 2.83;
        if(this.behavior != 'eat' && this.behavior != 'death'){
            this.anim.play('wild_boar_eat');
            this.behavior = 'eat';
        }
        if(this.state != 'stop'){
            let random = Math.random();
            if(random > 0.5){
                this.scheduleOnce(this.idle_behavior, time);
            }
            else{
                this.scheduleOnce(this.walk_behavior, time);
            }
        }
    }
    private hurted_behavior(damage: number){
        //let damage = (Math.random() * 20) / 1;
        if(this.health - damage * 2 > 0){
            this.schedule(()=>{
                this.health -= 2;
            }, 1 / damage, damage);
            this.scheduleOnce(()=>{this.hurted_enable = true;}, 1);
        }
        else{
            this.schedule(()=>{
                if(this.health > 2) this.health -= 2;
                else{
                    this.health = 0;
                    this.unscheduleAllCallbacks();
                    this.death_behavior();
                }
            }, 1 / damage, damage);
        }
    }
    private death_behavior(){
        if(this.behavior != 'death'){
            this.anim.play('wild_boar_death');
            this.behavior = 'death';
            cc.audioEngine.playEffect(this.death_sound, false);
            this.attack_particle.stopSystem();
            this.unscheduleAllCallbacks();
            this.scheduleOnce(()=>{ this.node.runAction(cc.sequence(cc.spawn(cc.fadeOut(3), cc.callFunc(()=>{this.death_particle.resetSystem();})), cc.callFunc(()=>{this.node.destroy();})));}, 3.0);
        }
    }

    onBeginContact(contact, self, other) {
        if(other.node.name == 'rabbit' || other.node.name == 'deer_female' || other.node.name == 'bear' || other.node.name == 'Player'){
            contact.disabled = true;
        }
        if(other.tag == 1000){
            contact.disabled = true;
            //Interaction with the Player
            if(other.node.getComponent('PlayerController').isAttacking != 0 && this.hurted_enable == true && this.behavior != 'death'){
                this.hurted_enable = false;
                this.hurted_behavior((Math.random() * 20) / 1);
            }
        }
        if(other.tag == 1001 && this.hurted_enable == true && this.behavior != 'death'){
            this.hurted_enable = false;
            this.hurted_behavior((Math.random() * 20) / 1);
        }
        if(other.tag == 1002){
            //this.hurted_enable = false;
            this.unscheduleAllCallbacks();
            this.hurted_behavior(40);
        }
    }
}
