// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Hawk extends cc.Component {

    @property(cc.Node)
    Player: cc.Node = null;
    
    @property()
    distance: number = 400;

    @property()
    speed: number = 100;

    @property(cc.ParticleSystem)
    charge: cc.ParticleSystem = null;

    @property(cc.ParticleSystem)
    death_particle: cc.ParticleSystem = null;

    @property(cc.AudioClip)
    attack_sound: cc.AudioClip = null;

    @property()
    width: number = 300;

    @property()
    height: number = 300;

    @property()
    x_offset: number = 0;

    @property()
    y_offset: number = 0;

    private state: string = 'stop';
    private anim: cc.Animation = null;
    private fly_action: cc.Action = null;
    private attack_action: cc.Action = null;
    private attack_enabled: boolean = true;
    public behavior: string = '';
    private direction: boolean = false;
    private left_bound: number = 0;
    private right_bound: number = 0;
    private origin_y: number = 0;
    private detect_left: number = 0;
    private detect_right: number = 0;
    private detect_top: number = 0;
    private detect_bottom: number = 0;
    start () {
        this.anim = this.getComponent(cc.Animation);
        this.left_bound = this.node.x - this.distance;
        this.right_bound = this.node.x + this.distance;
        this.origin_y = this.node.y;
        this.detect_left = this.node.x - this.width / 2 - this.x_offset;
        this.detect_right = this.node.x  + this.width / 2 - this.x_offset;
        this.detect_top = this.node.y + this.height / 2 + this.y_offset;
        this.detect_bottom = this.node.y - this.height / 2 + this.y_offset;
    }

    update (dt) {
        if(this.state == 'stop'){
            if(this.Player.y < this.detect_top && this.Player.y > this.detect_bottom){
                if(this.Player.x < this.detect_right && this.Player.x > this.detect_left){
                    this.state = 'active';
                    this.anim.play('hawk_fly');
                    this.fly_behavior();
                }
            }
        }
        if(this.state == 'active' && this.behavior != 'death'){
            if(this.Player.y > this.detect_top || this.Player.y < this.detect_bottom || this.Player.x > this.detect_right || this.Player.x < this.detect_left){
                this.anim.stop();
                this.state = 'stop';
                this.attack_enabled = true;
                this.charge.stopSystem();
                this.unscheduleAllCallbacks();
                this.node.stopAllActions();
            }
        }
        if(Math.abs(this.Player.x - this.node.x) < 500 && Math.abs(this.Player.y - this.node.y) < 400 &&  this.state == 'active' && this.attack_enabled == true){
            this.attack_behavior();
        }
     }

    private fly_behavior(){
        if(this.direction == false){
            this.node.scaleX = -1;
            this.fly_action = cc.sequence(cc.moveTo(Math.abs(this.left_bound-this.node.x)/this.speed, this.left_bound, this.origin_y), cc.callFunc(function(){ this.direction = true; this.fly_behavior();}, this));
        }
        else{
            this.node.scaleX = 1;
            this.fly_action = cc.sequence(cc.moveTo(Math.abs(this.right_bound-this.node.x)/this.speed, this.right_bound, this.origin_y), cc.callFunc(function(){ this.direction = false; this.fly_behavior();}, this));
        }
        this.node.runAction(this.fly_action);
    }
    private attack_behavior(){
        this.attack_enabled = false;
        cc.audioEngine.playEffect(this.attack_sound, false);
        this.node.stopAction(this.fly_action);
        this.charge.resetSystem();
        this.attack_action = cc.sequence(cc.callFunc(()=>{
            if(this.node.x < this.Player.x) this.node.scaleX = 1;
            else this.node.scaleX = -1;
        }, this), 
        cc.moveTo(1, this.Player.x, this.Player.y), cc.callFunc(()=>{
            if(this.node.scaleX == 1)   this.node.scaleX = -1;
            else this.node.scaleX = 1;
        }, this),
        cc.moveTo(1, this.node.x, this.node.y), 
        cc.callFunc(()=>{this.charge.stopSystem(); this.fly_behavior(); this.scheduleOnce(()=>{this.attack_enabled = true;}, 7);}));
        this.node.runAction(this.attack_action);
    }
    onBeginContact(contact, self, other) {
        /*  Interaction with the Player*/
        if(other.tag == 1000 && this.behavior != 'death'){
            if(other.node.getComponent('PlayerController').isAttacking != 0){
                this.behavior = 'death';
                this.unscheduleAllCallbacks();
                this.node.stopAllActions();
                this.anim.stop();
                this.charge.stopSystem();
                this.attack_enabled = false;
                this.scheduleOnce(()=>{this.node.runAction(cc.sequence(cc.spawn(cc.fadeOut(3), cc.callFunc(()=>{this.death_particle.resetSystem()})), cc.callFunc(()=>{this.node.destroy();})));}, 1);
            }
        }
        if(other.tag == 1001 && this.behavior != 'death'){
            this.behavior = 'death';
            this.unscheduleAllCallbacks();
            this.node.stopAllActions();
            this.anim.stop();
            this.charge.stopSystem();
            this.attack_enabled = false;
            this.scheduleOnce(()=>{this.node.runAction(cc.sequence(cc.spawn(cc.fadeOut(3), cc.callFunc(()=>{this.death_particle.resetSystem()})), cc.callFunc(()=>{this.node.destroy();})));}, 1);
        }
    }
}
