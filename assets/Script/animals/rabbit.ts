// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Rabbit extends cc.Component {

    @property(cc.Node)
    Player: cc.Node = null;

    @property()
    limit: number = 400;
    
    @property()
    width: number = 300;

    @property()
    height: number = 300;

    @property()
    x_offset: number = 0;

    @property()
    y_offset: number = 0;
    private anim: cc.Animation = null;
    private state: string = 'stop';
    private behavior: string = '';
    private direction: number = 1;
    private distance: number = 0;
    private detect_left: number = 0;
    private detect_right: number = 0;
    private detect_top: number = 0;
    private detect_bottom: number = 0;
    start () {
        this.anim = this.getComponent(cc.Animation);
        this.detect_left = this.node.x - this.width / 2 - this.x_offset;
        this.detect_right = this.node.x  + this.width / 2 - this.x_offset;
        this.detect_top = this.node.y + this.height / 2 + this.y_offset;
        this.detect_bottom = this.node.y - this.height / 2 + this.y_offset;
    }
    update (dt) {
        if(this.state == 'stop'){
            if(this.Player.y < this.detect_top && this.Player.y > this.detect_bottom){
                if(this.Player.x < this.detect_right && this.Player.x > this.detect_left){
                    this.state = 'active';
                    this.idle_behavior();
                }
            }
        }
        if(this.state == 'active'){
            if(this.Player.y > this.detect_top || this.Player.y < this.detect_bottom || this.Player.x > this.detect_right || this.Player.x < this.detect_left){
                this.state = 'stop';
                this.behavior = '';
                this.unscheduleAllCallbacks();
                this.anim.stop();
            }
        }
        if(this.behavior == 'run'){
            this.node.x += this.direction;
            this.distance += 2 * this.direction;
        }
        else if(this.behavior == 'walk'){
            this.node.x += 0.5 * this.direction;
            this.distance += this.direction;
        }
        if(this.distance >= this.limit){
            this.unscheduleAllCallbacks();
            this.distance = this.limit - 1;
            this.idle_behavior();
        }
        else if(this.distance <= -this.limit){
            this.unscheduleAllCallbacks();
            this.distance = -this.limit + 1;
            this.idle_behavior();
        }
    }
    private idle_behavior(){
        let time = ((Math.random() * 7.5) / 2.3) * 2.3 + 2.3;
        if(this.behavior != 'idle'){
            this.anim.play('rabbit_idle');
            this.behavior = 'idle';
        }
        if(this.state != 'stop'){
            if(Math.random() > 0.5){
                this.scheduleOnce(this.run_behavior, time);
            }
            else{
                this.scheduleOnce(this.walk_behavior, time);
            }
        }
    }
    private run_behavior(){
        let time = ((Math.random() * 6) / 0.75) * 0.75 + 1.5;
        if(this.behavior != 'run'){
            this.anim.play('rabbit_run');
            this.behavior = 'run';
            if(this.distance >= this.limit - 1){
                this.direction = -1;
                this.node.scaleX = 1;
            }
            else if(this.distance <= -this.limit + 1){
                this.direction = 1;
                this.node.scaleX = -1;   
            }
            else{
                if(Math.random() > 0.5){
                    this.direction = -1;
                    this.node.scaleX = 1;
                }
                else{
                    this.direction = 1;
                    this.node.scaleX = -1;                
                }
            }
        }        
        if(this.state != 'stop'){
            if(Math.random() > 0.5){
                this.scheduleOnce(this.idle_behavior, time);
            }
            else{
                this.scheduleOnce(this.walk_behavior, time);
            }
        }        
    }
    private walk_behavior(){
        let time = ((Math.random() * 6) / 0.70) * 0.70 + 1.4;
        if(this.behavior != 'walk'){
            this.anim.play('rabbit_walk');
            this.behavior = 'walk';
            if(this.distance >= this.limit - 1){
                this.direction = -1;
                this.node.scaleX = 1;
            }
            else if(this.distance <= -this.limit + 1){
                this.direction = 1;
                this.node.scaleX = -1;   
            }
            else{
                if(Math.random() > 0.5){
                    this.direction = -1;
                    this.node.scaleX = 1;
                }
                else{
                    this.direction = 1;
                    this.node.scaleX = -1;                
                }
            }
        }
        if(this.state != 'stop'){
            if(Math.random() > 0.5){
                this.scheduleOnce(this.run_behavior, time);
            }
            else{
                this.scheduleOnce(this.idle_behavior, time);
            }
        }        
    }
    onBeginContact(contact, self, other) {
        if(other.node.name == 'deer_female' || other.node.name == 'Player' || other.node.name == 'bear' || other.node.name == 'wild_boar' || other.node.name == 'rabbit'){
            contact.disabled = true;
        }
    }
}
