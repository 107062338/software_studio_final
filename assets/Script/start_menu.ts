// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property({type: cc.AudioClip})
    start_menu_bgm: cc.AudioClip = null;

    @property({type: cc.AudioClip})
    enter_sound: cc.AudioClip = null;

    @property(cc.Node)
    fog_1: cc.Node = null;

    @property(cc.Node)
    fog_2: cc.Node = null;

    @property(cc.Node)
    game_title: cc.Node = null;

    @property(cc.Node)
    start_btn: cc.Node = null;

    is_start_enable = false;

    start () {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyDown, this);
        cc.audioEngine.playMusic(this.start_menu_bgm, true);


        this.title_animation();

        this.scheduleOnce(() => {
            this.fog_animation();

            cc.director.preloadScene("map", () => {
                cc.log("map scene is loaded!");
            });
        }, 3);
    }

    onKeyDown(event) {

        if (!this.is_start_enable) { return; }
        if (event.keyCode == cc.macro.KEY.enter) {

            cc.audioEngine.playEffect(this.enter_sound, false);

            this.schedule(() => {
                cc.audioEngine.stop(0);
                cc.director.loadScene("map");
            }, 2);
        }
    }

    fog_animation() {
        var fog_1_action_r = cc.moveTo(9, cc.v2(750, 115));
        var fog_1_action_l = cc.moveTo(8, cc.v2(350, 115));
        let fog_1_action = cc.repeatForever(cc.sequence(fog_1_action_r, fog_1_action_l));

        this.fog_1.runAction(fog_1_action);

        var fog_2_action_r = cc.moveTo(7, cc.v2(650, 322));
        var fog_2_action_l = cc.moveTo(10, cc.v2(310, 322));
        let fog_2_action = cc.repeatForever(cc.sequence(fog_2_action_r, fog_2_action_l));

        this.fog_2.runAction(fog_2_action);
    }

    title_animation() {
        var title_action = cc.moveTo(4, cc.v2(480, 540));
        this.game_title.runAction(title_action);

        this.schedule(() => {
            this.start_btn.opacity = 255;

            var start_btn_action_fadein = cc.fadeIn(1.5);
            var start_btn_action_fadeout = cc.fadeOut(1.5);
            var start_btn_action = cc.repeatForever(cc.sequence(start_btn_action_fadein, start_btn_action_fadeout));

            this.start_btn.runAction(start_btn_action);
            this.is_start_enable = true;
        }, 5);
    }
} 
