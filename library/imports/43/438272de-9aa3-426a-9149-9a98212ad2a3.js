"use strict";
cc._RF.push(module, '43827LemqNCapFJmpghKtKj', 'target');
// Script/animals/wolf_attack/target.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Traget = /** @class */ (function (_super) {
    __extends(Traget, _super);
    function Traget() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Traget.prototype.start = function () {
        var _this = this;
        this.scheduleOnce(function () { _this.node.destroy(); }, 2);
    };
    Traget = __decorate([
        ccclass
    ], Traget);
    return Traget;
}(cc.Component));
exports.default = Traget;

cc._RF.pop();