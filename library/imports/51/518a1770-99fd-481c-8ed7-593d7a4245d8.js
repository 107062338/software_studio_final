"use strict";
cc._RF.push(module, '518a1dwmf1IHI7XWT16QkXY', 'first_attack');
// Script/animals/wolf_attack/first_attack.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var First_attack = /** @class */ (function (_super) {
    __extends(First_attack, _super);
    function First_attack() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    First_attack.prototype.start = function () {
        //this.scheduleOnce(()=>{this.node.destroy(), 10});
    };
    First_attack = __decorate([
        ccclass
    ], First_attack);
    return First_attack;
}(cc.Component));
exports.default = First_attack;

cc._RF.pop();