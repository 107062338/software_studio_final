"use strict";
cc._RF.push(module, 'ed275c6CV1EtJ2j96OgxJ/x', 'CameraController');
// Ninja/Script/CameraController.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CameraController = /** @class */ (function (_super) {
    __extends(CameraController, _super);
    function CameraController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.player = null;
        _this.boss = null;
        _this.deer_male = null;
        _this.GameManagerSystem = null;
        _this.isMoving = false;
        _this.isDeath = false;
        _this.isFinished = false;
        _this.isTrigger = false;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    CameraController.prototype.start = function () {
        this.GameManagerSystem = this.getComponent("GameManager");
    };
    CameraController.prototype.update = function (dt) {
        var _this = this;
        if (!this.isTrigger) {
            if (!this.isFinished) {
                this.node.x = this.player.x;
                this.node.y = this.player.y;
            }
            // detect boundary
            // todo
        }
        else {
            if (!this.isMoving) {
                this.isMoving = true;
                this.node.runAction(cc.moveTo(3, this.boss.x, this.boss.y));
            }
            this.scheduleOnce(function () {
                _this.boss.getComponent("wolf").state = "active";
            }, 3);
            this.scheduleOnce(function () {
                _this.isMoving = false;
                _this.isTrigger = false;
                _this.GameManagerSystem.GameMode = 0;
            }, 8);
        }
        if (!this.isFinished) {
            if (this.isDeath == false && this.boss.getComponent("wolf").behavior == "death") {
                this.isFinished = true;
                this.isDeath = true;
                this.boss.getComponent("wolf").play_death_animation();
                this.scheduleOnce(function () {
                    _this.node.runAction(cc.sequence(cc.moveTo(3, _this.deer_male.x, _this.deer_male.y), cc.callFunc(function () {
                        _this.deer_male.getComponent('deer_male').deer_male_anim();
                    })));
                }, 7);
                this.scheduleOnce(function () { cc.director.loadScene('start'); }, 22);
            }
        }
    };
    __decorate([
        property(cc.Node)
    ], CameraController.prototype, "player", void 0);
    __decorate([
        property(cc.Node)
    ], CameraController.prototype, "boss", void 0);
    __decorate([
        property(cc.Node)
    ], CameraController.prototype, "deer_male", void 0);
    CameraController = __decorate([
        ccclass
    ], CameraController);
    return CameraController;
}(cc.Component));
exports.default = CameraController;

cc._RF.pop();