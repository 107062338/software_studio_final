"use strict";
cc._RF.push(module, '30016LqNt5KE68cwE1f2XS2', 'rabbit');
// Script/animals/rabbit.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Rabbit = /** @class */ (function (_super) {
    __extends(Rabbit, _super);
    function Rabbit() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.Player = null;
        _this.limit = 400;
        _this.width = 300;
        _this.height = 300;
        _this.x_offset = 0;
        _this.y_offset = 0;
        _this.anim = null;
        _this.state = 'stop';
        _this.behavior = '';
        _this.direction = 1;
        _this.distance = 0;
        _this.detect_left = 0;
        _this.detect_right = 0;
        _this.detect_top = 0;
        _this.detect_bottom = 0;
        return _this;
    }
    Rabbit.prototype.start = function () {
        this.anim = this.getComponent(cc.Animation);
        this.detect_left = this.node.x - this.width / 2 - this.x_offset;
        this.detect_right = this.node.x + this.width / 2 - this.x_offset;
        this.detect_top = this.node.y + this.height / 2 + this.y_offset;
        this.detect_bottom = this.node.y - this.height / 2 + this.y_offset;
    };
    Rabbit.prototype.update = function (dt) {
        if (this.state == 'stop') {
            if (this.Player.y < this.detect_top && this.Player.y > this.detect_bottom) {
                if (this.Player.x < this.detect_right && this.Player.x > this.detect_left) {
                    this.state = 'active';
                    this.idle_behavior();
                }
            }
        }
        if (this.state == 'active') {
            if (this.Player.y > this.detect_top || this.Player.y < this.detect_bottom || this.Player.x > this.detect_right || this.Player.x < this.detect_left) {
                this.state = 'stop';
                this.behavior = '';
                this.unscheduleAllCallbacks();
                this.anim.stop();
            }
        }
        if (this.behavior == 'run') {
            this.node.x += this.direction;
            this.distance += 2 * this.direction;
        }
        else if (this.behavior == 'walk') {
            this.node.x += 0.5 * this.direction;
            this.distance += this.direction;
        }
        if (this.distance >= this.limit) {
            this.unscheduleAllCallbacks();
            this.distance = this.limit - 1;
            this.idle_behavior();
        }
        else if (this.distance <= -this.limit) {
            this.unscheduleAllCallbacks();
            this.distance = -this.limit + 1;
            this.idle_behavior();
        }
    };
    Rabbit.prototype.idle_behavior = function () {
        var time = ((Math.random() * 7.5) / 2.3) * 2.3 + 2.3;
        if (this.behavior != 'idle') {
            this.anim.play('rabbit_idle');
            this.behavior = 'idle';
        }
        if (this.state != 'stop') {
            if (Math.random() > 0.5) {
                this.scheduleOnce(this.run_behavior, time);
            }
            else {
                this.scheduleOnce(this.walk_behavior, time);
            }
        }
    };
    Rabbit.prototype.run_behavior = function () {
        var time = ((Math.random() * 6) / 0.75) * 0.75 + 1.5;
        if (this.behavior != 'run') {
            this.anim.play('rabbit_run');
            this.behavior = 'run';
            if (this.distance >= this.limit - 1) {
                this.direction = -1;
                this.node.scaleX = 1;
            }
            else if (this.distance <= -this.limit + 1) {
                this.direction = 1;
                this.node.scaleX = -1;
            }
            else {
                if (Math.random() > 0.5) {
                    this.direction = -1;
                    this.node.scaleX = 1;
                }
                else {
                    this.direction = 1;
                    this.node.scaleX = -1;
                }
            }
        }
        if (this.state != 'stop') {
            if (Math.random() > 0.5) {
                this.scheduleOnce(this.idle_behavior, time);
            }
            else {
                this.scheduleOnce(this.walk_behavior, time);
            }
        }
    };
    Rabbit.prototype.walk_behavior = function () {
        var time = ((Math.random() * 6) / 0.70) * 0.70 + 1.4;
        if (this.behavior != 'walk') {
            this.anim.play('rabbit_walk');
            this.behavior = 'walk';
            if (this.distance >= this.limit - 1) {
                this.direction = -1;
                this.node.scaleX = 1;
            }
            else if (this.distance <= -this.limit + 1) {
                this.direction = 1;
                this.node.scaleX = -1;
            }
            else {
                if (Math.random() > 0.5) {
                    this.direction = -1;
                    this.node.scaleX = 1;
                }
                else {
                    this.direction = 1;
                    this.node.scaleX = -1;
                }
            }
        }
        if (this.state != 'stop') {
            if (Math.random() > 0.5) {
                this.scheduleOnce(this.run_behavior, time);
            }
            else {
                this.scheduleOnce(this.idle_behavior, time);
            }
        }
    };
    Rabbit.prototype.onBeginContact = function (contact, self, other) {
        if (other.node.name == 'deer_female' || other.node.name == 'Player' || other.node.name == 'bear' || other.node.name == 'wild_boar' || other.node.name == 'rabbit') {
            contact.disabled = true;
        }
    };
    __decorate([
        property(cc.Node)
    ], Rabbit.prototype, "Player", void 0);
    __decorate([
        property()
    ], Rabbit.prototype, "limit", void 0);
    __decorate([
        property()
    ], Rabbit.prototype, "width", void 0);
    __decorate([
        property()
    ], Rabbit.prototype, "height", void 0);
    __decorate([
        property()
    ], Rabbit.prototype, "x_offset", void 0);
    __decorate([
        property()
    ], Rabbit.prototype, "y_offset", void 0);
    Rabbit = __decorate([
        ccclass
    ], Rabbit);
    return Rabbit;
}(cc.Component));
exports.default = Rabbit;

cc._RF.pop();