"use strict";
cc._RF.push(module, 'ba66ejazS5PyrGLZywSMCvm', 'GameManager');
// Ninja/Script/GameManager.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var GameManager = /** @class */ (function (_super) {
    __extends(GameManager, _super);
    function GameManager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.Player = null;
        _this.BlackScreen = null;
        _this.GameBgm = null;
        _this.BossBgm = null;
        _this.PlayerStatusSystem = null;
        _this.PlayerControllerSystem = null;
        _this.CameraController = null;
        _this.Timer = null;
        _this.GameMode = 0;
        return _this;
    }
    /*
        0: GamePlay
        1: GamePause
    */
    // LIFE-CYCLE CALLBACKS:
    GameManager.prototype.onLoad = function () {
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getPhysicsManager().gravity = new cc.Vec2(0, -1000);
    };
    GameManager.prototype.start = function () {
        this.PlayerStatusSystem = this.Player.getComponent("PlayerStatusSystem");
        this.PlayerControllerSystem = this.Player.getComponent("PlayerController");
        this.CameraController = this.getComponent("CameraController");
        this.BlackScreen.opacity = 0;
        this.BlackScreen.active = false;
        cc.audioEngine.playMusic(this.GameBgm, true);
    };
    GameManager.prototype.update = function (dt) {
        if (this.PlayerStatusSystem.isDead) {
            this.Timer += dt;
        }
    };
    GameManager.prototype.GameOver = function () {
        //cc.audioEngine.stopAll();
        this.BlackScreen.active = true;
        this.scheduleOnce(function () {
            cc.director.loadScene("start");
        }, 5);
        if (this.Timer <= 5) {
            var alpha = Math.floor(this.Timer / 5 * 255);
            this.BlackScreen.opacity = alpha;
        }
    };
    GameManager.prototype.GameWin = function () {
    };
    GameManager.prototype.BossTrigger = function () {
        this.GameMode = 1;
        this.CameraController.isTrigger = true;
        this.PlayerControllerSystem.Flower = 0;
        cc.audioEngine.stopMusic();
        cc.audioEngine.playMusic(this.BossBgm, true);
    };
    __decorate([
        property(cc.Node)
    ], GameManager.prototype, "Player", void 0);
    __decorate([
        property(cc.Node)
    ], GameManager.prototype, "BlackScreen", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], GameManager.prototype, "GameBgm", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], GameManager.prototype, "BossBgm", void 0);
    GameManager = __decorate([
        ccclass
    ], GameManager);
    return GameManager;
}(cc.Component));
exports.default = GameManager;

cc._RF.pop();