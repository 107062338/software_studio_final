"use strict";
cc._RF.push(module, '827764SvWlGTpEGsrxKkd/6', 'second_attack');
// Script/animals/wolf_attack/second_attack.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Second_attack = /** @class */ (function (_super) {
    __extends(Second_attack, _super);
    function Second_attack() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    Second_attack.prototype.start = function () {
        var _this = this;
        this.scheduleOnce(function () { _this.node.destroy(); }, 10);
    };
    Second_attack = __decorate([
        ccclass
    ], Second_attack);
    return Second_attack;
}(cc.Component));
exports.default = Second_attack;

cc._RF.pop();