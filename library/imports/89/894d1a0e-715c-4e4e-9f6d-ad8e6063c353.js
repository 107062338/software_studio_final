"use strict";
cc._RF.push(module, '894d1oOcVxOTp9trY5gY8NT', 'Skill1Controller');
// Ninja/Script/Skill1Controller.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Skill1Controller = /** @class */ (function (_super) {
    __extends(Skill1Controller, _super);
    function Skill1Controller() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.Skill1Particle = null;
        _this.Direction = 0; // 1: right -1: left
        _this.isCollided = false;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    Skill1Controller.prototype.start = function () {
        this.schedule(this.Instantiate, 0.01);
    };
    Skill1Controller.prototype.update = function (dt) {
        var _this = this;
        if (!this.isCollided) {
            this.node.x += this.Direction * 1000 * dt;
        }
        else {
            this.scheduleOnce(function () {
                _this.unschedule(_this.Instantiate);
                _this.node.destroy();
            }, 1);
        }
    };
    Skill1Controller.prototype.onBeginContact = function (contact, self, other) {
        if (other.tag != 0 && other.tag != 1000) {
            if (other.tag == 1) {
                if (!((contact.getWorldManifold().normal.x == 0 || contact.getWorldManifold().normal.x == -0) && contact.getWorldManifold().normal.y == -1)) {
                    this.isCollided = true;
                }
            }
            else if (other.tag != 3 && other.tag != 4 && other.tag != 100) {
                this.isCollided = true;
            }
        }
    };
    Skill1Controller.prototype.Instantiate = function () {
        if (this.isCollided) {
            return;
        }
        var Skill1 = cc.instantiate(this.Skill1Particle);
        Skill1.parent = this.node.parent;
        Skill1.position = this.node.position;
        this.scheduleOnce(function () {
            Skill1.destroy();
        }, 0.5);
    };
    __decorate([
        property(cc.Prefab)
    ], Skill1Controller.prototype, "Skill1Particle", void 0);
    Skill1Controller = __decorate([
        ccclass
    ], Skill1Controller);
    return Skill1Controller;
}(cc.Component));
exports.default = Skill1Controller;

cc._RF.pop();