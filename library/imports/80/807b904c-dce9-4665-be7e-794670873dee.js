"use strict";
cc._RF.push(module, '807b9BM3OlGZb5+eUZwhz3u', 'wild_boar');
// Script/animals/wild_boar.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Wild_boar = /** @class */ (function (_super) {
    __extends(Wild_boar, _super);
    function Wild_boar() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.Player = null;
        _this.limit = 400;
        _this.death_particle = null;
        _this.attack_particle = null;
        _this.attack_sound = null;
        _this.death_sound = null;
        _this.width = 300;
        _this.height = 300;
        _this.x_offset = 0;
        _this.y_offset = 0;
        _this.anim = null;
        _this.state = 'stop';
        _this.behavior = '';
        _this.direction = 1;
        _this.distance = 0;
        _this.hunt = true;
        _this.hurted_enable = true;
        _this.health = 80;
        _this.detect_left = 0;
        _this.detect_right = 0;
        _this.detect_top = 0;
        _this.detect_bottom = 0;
        return _this;
    }
    Wild_boar.prototype.start = function () {
        this.anim = this.getComponent(cc.Animation);
        this.detect_left = this.node.x - this.width / 2 - this.x_offset;
        this.detect_right = this.node.x + this.width / 2 - this.x_offset;
        this.detect_top = this.node.y + this.height / 2 + this.y_offset;
        this.detect_bottom = this.node.y - this.height / 2 + this.y_offset;
        this.life_bar = this.node.getChildByName('wild_boar_life');
    };
    Wild_boar.prototype.update = function (dt) {
        if (this.node.scaleX == -1) { //right
            this.life_bar.x = (80 - this.health) / 2;
            this.life_bar.width = this.health;
            this.life_bar.angle = this.node.angle;
        }
        else { //left
            this.life_bar.x = -(80 - this.health) / 2;
            this.life_bar.width = this.health;
            this.life_bar.angle = -this.node.angle;
        }
        if (this.state == 'stop') {
            if (this.Player.y < this.detect_top && this.Player.y > this.detect_bottom) {
                if (this.Player.x < this.detect_right && this.Player.x > this.detect_left) {
                    this.state = 'active';
                    this.idle_behavior();
                }
            }
        }
        if (this.state == 'active' && this.behavior != 'death') {
            if (this.Player.y > this.detect_top || this.Player.y < this.detect_bottom || this.Player.x > this.detect_right || this.Player.x < this.detect_left) {
                this.state = 'stop';
                this.behavior = '';
                this.hunt = true;
                this.anim.stop();
                this.unscheduleAllCallbacks();
            }
        }
        if (Math.abs(this.Player.x - this.node.x) < 250 && Math.abs(this.Player.y - this.node.y) < 250 && this.state == 'active' && this.hunt == true && this.behavior != 'death') {
            this.unscheduleAllCallbacks();
            this.run_behavior();
        }
        if (this.behavior == 'run') {
            this.node.x += this.direction;
            this.distance += 2 * this.direction;
        }
        else if (this.behavior == 'walk') {
            this.node.x += 0.5 * this.direction;
            this.distance += this.direction;
        }
        if (this.distance >= this.limit && this.behavior != 'death') {
            this.unscheduleAllCallbacks();
            if (this.behavior == 'run')
                this.attack_particle.stopSystem();
            this.distance = this.limit - 1;
            this.idle_behavior();
        }
        else if (this.distance <= -this.limit && this.behavior != 'death') {
            this.unscheduleAllCallbacks();
            if (this.behavior == 'run')
                this.attack_particle.stopSystem();
            this.distance = -this.limit + 1;
            this.idle_behavior();
        }
    };
    Wild_boar.prototype.idle_behavior = function () {
        var _this = this;
        this.hurted_enable = true;
        var time = Math.random() + 1.88;
        if (this.behavior != 'idle' && this.behavior != 'death') {
            this.anim.play('wild_boar_idle');
            this.behavior = 'idle';
            this.scheduleOnce(function () { _this.hunt = true; }, 7);
        }
        if (this.state != 'stop') {
            var random = Math.random();
            if (random > 0.5) {
                this.scheduleOnce(this.walk_behavior, time);
            }
            else {
                this.scheduleOnce(this.eat_behavior, time);
            }
        }
    };
    Wild_boar.prototype.run_behavior = function () {
        if (this.behavior != 'run' && this.behavior != 'death' && this.hunt == true) {
            this.hurted_enable = true;
            this.anim.play('wild_boar_run');
            this.behavior = 'run';
            cc.audioEngine.playEffect(this.attack_sound, false);
            this.hunt = false;
            this.attack_particle.resetSystem();
            if (this.Player.x - this.node.x > 0) {
                this.direction = 1;
                this.node.scaleX = -1;
            }
            else {
                this.direction = -1;
                this.node.scaleX = 1;
            }
        }
    };
    Wild_boar.prototype.walk_behavior = function () {
        var time = ((Math.random() * 4) / 2.0) * 2.0 + 2.0;
        if (this.behavior != 'walk' && this.behavior != 'death') {
            this.anim.play('wild_boar_walk');
            this.behavior = 'walk';
            if (this.distance >= this.limit - 1) {
                this.direction = -1;
                this.node.scaleX = 1;
            }
            else if (this.distance <= -this.limit + 1) {
                this.direction = 1;
                this.node.scaleX = -1;
            }
            else {
                if (Math.random() > 0.5) {
                    this.direction = -1;
                    this.node.scaleX = 1;
                }
                else {
                    this.direction = 1;
                    this.node.scaleX = -1;
                }
            }
        }
        if (this.state != 'stop') {
            var random = Math.random();
            if (random > 0.5) {
                this.scheduleOnce(this.idle_behavior, time);
            }
            else {
                this.scheduleOnce(this.eat_behavior, time);
            }
        }
    };
    Wild_boar.prototype.eat_behavior = function () {
        var time = Math.random() + 2.83;
        if (this.behavior != 'eat' && this.behavior != 'death') {
            this.anim.play('wild_boar_eat');
            this.behavior = 'eat';
        }
        if (this.state != 'stop') {
            var random = Math.random();
            if (random > 0.5) {
                this.scheduleOnce(this.idle_behavior, time);
            }
            else {
                this.scheduleOnce(this.walk_behavior, time);
            }
        }
    };
    Wild_boar.prototype.hurted_behavior = function (damage) {
        var _this = this;
        //let damage = (Math.random() * 20) / 1;
        if (this.health - damage * 2 > 0) {
            this.schedule(function () {
                _this.health -= 2;
            }, 1 / damage, damage);
            this.scheduleOnce(function () { _this.hurted_enable = true; }, 1);
        }
        else {
            this.schedule(function () {
                if (_this.health > 2)
                    _this.health -= 2;
                else {
                    _this.health = 0;
                    _this.unscheduleAllCallbacks();
                    _this.death_behavior();
                }
            }, 1 / damage, damage);
        }
    };
    Wild_boar.prototype.death_behavior = function () {
        var _this = this;
        if (this.behavior != 'death') {
            this.anim.play('wild_boar_death');
            this.behavior = 'death';
            cc.audioEngine.playEffect(this.death_sound, false);
            this.attack_particle.stopSystem();
            this.unscheduleAllCallbacks();
            this.scheduleOnce(function () { _this.node.runAction(cc.sequence(cc.spawn(cc.fadeOut(3), cc.callFunc(function () { _this.death_particle.resetSystem(); })), cc.callFunc(function () { _this.node.destroy(); }))); }, 3.0);
        }
    };
    Wild_boar.prototype.onBeginContact = function (contact, self, other) {
        if (other.node.name == 'rabbit' || other.node.name == 'deer_female' || other.node.name == 'bear' || other.node.name == 'Player') {
            contact.disabled = true;
        }
        if (other.tag == 1000) {
            contact.disabled = true;
            //Interaction with the Player
            if (other.node.getComponent('PlayerController').isAttacking != 0 && this.hurted_enable == true && this.behavior != 'death') {
                this.hurted_enable = false;
                this.hurted_behavior((Math.random() * 20) / 1);
            }
        }
        if (other.tag == 1001 && this.hurted_enable == true && this.behavior != 'death') {
            this.hurted_enable = false;
            this.hurted_behavior((Math.random() * 20) / 1);
        }
        if (other.tag == 1002) {
            //this.hurted_enable = false;
            this.unscheduleAllCallbacks();
            this.hurted_behavior(40);
        }
    };
    __decorate([
        property(cc.Node)
    ], Wild_boar.prototype, "Player", void 0);
    __decorate([
        property()
    ], Wild_boar.prototype, "limit", void 0);
    __decorate([
        property(cc.ParticleSystem)
    ], Wild_boar.prototype, "death_particle", void 0);
    __decorate([
        property(cc.ParticleSystem)
    ], Wild_boar.prototype, "attack_particle", void 0);
    __decorate([
        property(cc.AudioClip)
    ], Wild_boar.prototype, "attack_sound", void 0);
    __decorate([
        property(cc.AudioClip)
    ], Wild_boar.prototype, "death_sound", void 0);
    __decorate([
        property()
    ], Wild_boar.prototype, "width", void 0);
    __decorate([
        property()
    ], Wild_boar.prototype, "height", void 0);
    __decorate([
        property()
    ], Wild_boar.prototype, "x_offset", void 0);
    __decorate([
        property()
    ], Wild_boar.prototype, "y_offset", void 0);
    Wild_boar = __decorate([
        ccclass
    ], Wild_boar);
    return Wild_boar;
}(cc.Component));
exports.default = Wild_boar;

cc._RF.pop();