"use strict";
cc._RF.push(module, 'd0d7fhLmP5MSIesMYtS9S6l', 'wolf');
// Script/animals/wolf.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Wolf = /** @class */ (function (_super) {
    __extends(Wolf, _super);
    function Wolf() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.Player = null;
        _this.wolf_pre_repair = null;
        _this.wolf_repair = null;
        _this.first_attack_prefab = null;
        _this.second_attack_prefab = null;
        _this.third_attack_prefab = null;
        _this.wolf_howl_sound = null;
        _this.repair_sound = null;
        _this.first_sound = null;
        _this.third_sound = null;
        _this.target = null;
        _this.wolf_first_attack_particle = null;
        _this.wolf_second_attack_particle = null;
        _this.wolf_third_attack_particle = null;
        _this.wolf_death_particle = null;
        _this.wolf_hp = null;
        _this.state = 'stop'; // stop, active
        _this.behavior = 'start'; // begining, idle, walk, repair, preatk, first_attack, second_attack, third_attack, death
        _this.anim = null;
        _this.health = 200;
        _this.death_walk = false;
        _this.direction = 1;
        return _this;
    }
    Wolf.prototype.start = function () {
        this.wolf_hp.active = false;
        this.anim = this.getComponent(cc.Animation);
    };
    Wolf.prototype.update = function (dt) {
        var _this = this;
        this.wolf_hp.getComponent(cc.Sprite).fillRange = this.health / 200;
        if (this.state != 'stop') { //GameEngine set this.state = 'active';
            if (this.behavior == 'start') {
                this.behavior = 'begining';
                cc.audioEngine.playEffect(this.wolf_howl_sound, false);
                this.anim.play('wolf_begining'); // Call idle_behavior at the end
                this.scheduleOnce(this.idle_behavior, 6);
                this.scheduleOnce(function () {
                    _this.wolf_hp.active = true;
                }, 5);
            }
            else if (this.behavior == 'walk') {
                this.node.x += 2 * this.direction;
            }
        }
        if (this.health <= 0 && this.behavior != 'death') {
            this.health = 0;
            this.behavior = 'death';
            this.unscheduleAllCallbacks();
            //this.play_death_animation(); Gameengine calls this func after detecting that this.behavior == 'death' and the camera had focus on the wolf
        }
        if (this.death_walk == true) {
            this.node.x += -this.node.scaleX;
        }
    };
    Wolf.prototype.idle_behavior = function () {
        var _this = this;
        if (this.behavior != 'idle' && this.behavior != 'death') {
            this.behavior = 'idle';
            this.anim.play('wolf_idle');
            this.scheduleOnce(function () {
                var random = Math.random();
                if (random > 0.5)
                    _this.walk_behavior();
                else if (random > 0.4)
                    _this.repair_behavior();
                else
                    _this.preatk_behavior();
            }, 2);
        }
    };
    Wolf.prototype.walk_behavior = function () {
        var _this = this;
        if (this.behavior != 'walk' && this.behavior != 'death') {
            this.behavior = 'walk';
            this.anim.play('wolf_walk');
            if (Math.random() > 0.5) {
                this.direction = -1;
                this.node.scaleX = 1;
            }
            else {
                this.direction = 1;
                this.node.scaleX = -1;
            }
            this.scheduleOnce(function () {
                var random = Math.random();
                if (random > 0.4)
                    _this.idle_behavior();
                else if (random > 0.3)
                    _this.repair_behavior();
                else
                    _this.preatk_behavior();
            }, 1.55);
        }
    };
    Wolf.prototype.repair_behavior = function () {
        var _this = this;
        if (this.behavior != 'repair' && this.behavior != 'death') {
            this.behavior = 'repair';
            this.anim.play('wolf_howl');
            cc.audioEngine.playEffect(this.repair_sound, false);
            this.wolf_pre_repair.resetSystem();
            this.scheduleOnce(function () { _this.anim.play('wolf_howl'); }, 1.9);
            this.scheduleOnce(function () { _this.anim.play('wolf_howl'); }, 3.8);
            this.scheduleOnce(function () {
                if (_this.health < 100)
                    _this.health += 100;
                else
                    _this.health = 200;
                _this.wolf_repair.resetSystem();
                var random = Math.random();
                if (random > 0.75)
                    _this.idle_behavior();
                else if (random > 0.3)
                    _this.walk_behavior();
                else
                    _this.preatk_behavior();
            }, 5.7);
        }
    };
    Wolf.prototype.preatk_behavior = function () {
        var _this = this;
        if (this.behavior != 'death') {
            this.behavior = 'preatk';
            this.anim.play('wolf_attack');
            var random = Math.random();
            if (random > 0.67) {
                this.wolf_first_attack_particle.resetSystem();
                this.first_attack();
            }
            else if (random > 0.33) {
                this.wolf_second_attack_particle.resetSystem();
                this.second_attack();
            }
            else {
                this.wolf_third_attack_particle.resetSystem();
                this.third_attack();
            }
            this.scheduleOnce(function () {
                var random = Math.random();
                if (random > 0.9)
                    _this.repair_behavior();
                else if (random > 0.6)
                    _this.walk_behavior();
                else if (random > 0.1)
                    _this.idle_behavior();
                else
                    _this.preatk_behavior();
            }, 1.1);
        }
    };
    Wolf.prototype.first_attack = function () {
        var _this = this;
        cc.audioEngine.playEffect(this.first_sound, false);
        var tar = cc.instantiate(this.target);
        tar.x = this.Player.x;
        tar.y = this.Player.y;
        this.node.parent.addChild(cc.instantiate(tar));
        var attack = cc.instantiate(this.first_attack_prefab);
        attack.x = this.Player.x;
        attack.y = this.Player.y;
        this.scheduleOnce(function () {
            _this.node.parent.addChild(attack);
        }, 1.5);
    };
    Wolf.prototype.second_attack = function () {
        var _this = this;
        var tar = cc.instantiate(this.target);
        tar.x = this.Player.x;
        tar.y = this.Player.y;
        this.node.parent.addChild(cc.instantiate(tar));
        var attack = cc.instantiate(this.second_attack_prefab);
        attack.x = this.Player.x;
        attack.y = this.Player.y;
        this.scheduleOnce(function () {
            _this.node.parent.addChild(attack);
        }, 1.5);
    };
    Wolf.prototype.third_attack = function () {
        var _this = this;
        this.scheduleOnce(function () { cc.audioEngine.playEffect(_this.third_sound, false); }, 1);
        var tar = cc.instantiate(this.target);
        tar.x = this.Player.x;
        tar.y = this.Player.y;
        this.node.parent.addChild(cc.instantiate(tar));
        var attack = cc.instantiate(this.third_attack_prefab);
        attack.x = this.Player.x;
        attack.y = this.Player.y;
        this.scheduleOnce(function () {
            _this.node.parent.addChild(attack);
        }, 2);
    };
    Wolf.prototype.play_death_animation = function () {
        var _this = this;
        this.anim.play('wolf_death');
        this.scheduleOnce(function () { _this.wolf_death_particle.resetSystem(); }, 3);
        this.scheduleOnce(function () { _this.death_walk == true; }, 6);
        this.scheduleOnce(function () { _this.node.runAction(cc.sequence(cc.fadeOut(3), cc.callFunc(function () { _this.node.destroy(); }))); }, 5);
    };
    Wolf.prototype.onBeginContact = function (contact, self, other) {
        if (other.tag == 1001 && this.behavior != 'death') {
            contact.disabled = true;
            this.health -= Math.random() * 30 / 1;
            if (this.behavior == 'repair') {
                this.unscheduleAllCallbacks();
                this.wolf_pre_repair.stopSystem();
                this.idle_behavior();
            }
        }
        else if (other.tag == 1000 && this.behavior != 'death') {
            contact.disabled = true;
            if (other.node.getComponent('PlayerController').isAttacking != 0) {
                this.health -= Math.random() * 30 / 1;
                if (this.behavior == 'repair') {
                    this.unscheduleAllCallbacks();
                    this.wolf_pre_repair.stopSystem();
                    this.idle_behavior();
                }
            }
        }
        else if (other.tag == 1002 && this.behavior != 'death') {
            contact.disabled = true;
            this.health -= 100;
            if (this.behavior == 'repair') {
                this.unscheduleAllCallbacks();
                this.wolf_pre_repair.stopSystem();
                this.idle_behavior();
            }
        }
        if (other.node.name == 'Player') {
            contact.disabled = true;
        }
    };
    __decorate([
        property(cc.Node)
    ], Wolf.prototype, "Player", void 0);
    __decorate([
        property(cc.ParticleSystem)
    ], Wolf.prototype, "wolf_pre_repair", void 0);
    __decorate([
        property(cc.ParticleSystem)
    ], Wolf.prototype, "wolf_repair", void 0);
    __decorate([
        property(cc.Prefab)
    ], Wolf.prototype, "first_attack_prefab", void 0);
    __decorate([
        property(cc.Prefab)
    ], Wolf.prototype, "second_attack_prefab", void 0);
    __decorate([
        property(cc.Prefab)
    ], Wolf.prototype, "third_attack_prefab", void 0);
    __decorate([
        property(cc.AudioClip)
    ], Wolf.prototype, "wolf_howl_sound", void 0);
    __decorate([
        property(cc.AudioClip)
    ], Wolf.prototype, "repair_sound", void 0);
    __decorate([
        property(cc.AudioClip)
    ], Wolf.prototype, "first_sound", void 0);
    __decorate([
        property(cc.AudioClip)
    ], Wolf.prototype, "third_sound", void 0);
    __decorate([
        property(cc.Prefab)
    ], Wolf.prototype, "target", void 0);
    __decorate([
        property(cc.ParticleSystem)
    ], Wolf.prototype, "wolf_first_attack_particle", void 0);
    __decorate([
        property(cc.ParticleSystem)
    ], Wolf.prototype, "wolf_second_attack_particle", void 0);
    __decorate([
        property(cc.ParticleSystem)
    ], Wolf.prototype, "wolf_third_attack_particle", void 0);
    __decorate([
        property(cc.ParticleSystem)
    ], Wolf.prototype, "wolf_death_particle", void 0);
    __decorate([
        property(cc.Node)
    ], Wolf.prototype, "wolf_hp", void 0);
    Wolf = __decorate([
        ccclass
    ], Wolf);
    return Wolf;
}(cc.Component));
exports.default = Wolf;

cc._RF.pop();