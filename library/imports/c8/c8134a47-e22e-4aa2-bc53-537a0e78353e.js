"use strict";
cc._RF.push(module, 'c8134pH4i5KorxTU3oOeDU+', 'deer_female');
// Script/animals/deer_female.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Deer_female = /** @class */ (function (_super) {
    __extends(Deer_female, _super);
    function Deer_female() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.Player = null;
        _this.limit = 400;
        _this.width = 300;
        _this.height = 300;
        _this.x_offset = 0;
        _this.y_offset = 0;
        _this.anim = null;
        _this.state = 'stop';
        _this.behavior = '';
        _this.direction = 1;
        _this.distance = 0;
        _this.detect_left = 0;
        _this.detect_right = 0;
        _this.detect_top = 0;
        _this.detect_bottom = 0;
        return _this;
    }
    Deer_female.prototype.start = function () {
        this.anim = this.getComponent(cc.Animation);
        this.detect_left = this.node.x - this.width / 2 - this.x_offset;
        this.detect_right = this.node.x + this.width / 2 - this.x_offset;
        this.detect_top = this.node.y + this.height / 2 + this.y_offset;
        this.detect_bottom = this.node.y - this.height / 2 + this.y_offset;
    };
    Deer_female.prototype.update = function (dt) {
        if (this.state == 'stop') {
            if (this.Player.y < this.detect_top && this.Player.y > this.detect_bottom) {
                if (this.Player.x < this.detect_right && this.Player.x > this.detect_left) {
                    this.state = 'active';
                    this.idle_behavior();
                }
            }
        }
        if (this.state == 'active') {
            if (this.Player.y > this.detect_top || this.Player.y < this.detect_bottom || this.Player.x > this.detect_right || this.Player.x < this.detect_left) {
                this.state = 'stop';
                this.behavior = '';
                this.unscheduleAllCallbacks();
                this.anim.stop();
            }
        }
        if (this.behavior == 'run') {
            this.node.x += this.direction;
            this.distance += 2 * this.direction;
        }
        else if (this.behavior == 'walk') {
            this.node.x += 0.5 * this.direction;
            this.distance += this.direction;
        }
        if (this.distance >= this.limit) {
            this.unscheduleAllCallbacks();
            this.distance = this.limit - 1;
            this.idle_behavior();
        }
        else if (this.distance <= -this.limit) {
            this.unscheduleAllCallbacks();
            this.distance = -this.limit + 1;
            this.idle_behavior();
        }
    };
    Deer_female.prototype.idle_behavior = function () {
        var time = Math.random() * 1.5 + 3;
        if (this.behavior != 'idle') {
            this.anim.play('deer_idle_female');
            this.behavior = 'idle';
        }
        if (this.state != 'stop') {
            var random = Math.random();
            if (random > 0.67) {
                this.scheduleOnce(this.run_behavior, time);
            }
            else if (random > 0.33) {
                this.scheduleOnce(this.walk_behavior, time);
            }
            else {
                this.scheduleOnce(this.eat_behavior, time);
            }
        }
    };
    Deer_female.prototype.run_behavior = function () {
        var time = ((Math.random() * 5) / 1.14) * 1.14 + 1.14;
        if (this.behavior != 'run') {
            this.anim.play('deer_run_female');
            this.behavior = 'run';
            if (this.distance >= this.limit - 1) {
                this.direction = -1;
                this.node.scaleX = 1;
            }
            else if (this.distance <= -this.limit + 1) {
                this.direction = 1;
                this.node.scaleX = -1;
            }
            else {
                if (Math.random() > 0.5) {
                    this.direction = -1;
                    this.node.scaleX = 1;
                }
                else {
                    this.direction = 1;
                    this.node.scaleX = -1;
                }
            }
        }
        if (this.state != 'stop') {
            var random = Math.random();
            if (random > 0.67) {
                this.scheduleOnce(this.idle_behavior, time);
            }
            else if (random > 0.33) {
                this.scheduleOnce(this.walk_behavior, time);
            }
            else {
                this.scheduleOnce(this.eat_behavior, time);
            }
        }
    };
    Deer_female.prototype.walk_behavior = function () {
        var time = ((Math.random() * 5) / 0.94) * 0.94 + 0.94;
        if (this.behavior != 'walk') {
            this.anim.play('deer_walk_female');
            this.behavior = 'walk';
            if (this.distance >= this.limit - 1) {
                this.direction = -1;
                this.node.scaleX = 1;
            }
            else if (this.distance <= -this.limit + 1) {
                this.direction = 1;
                this.node.scaleX = -1;
            }
            else {
                if (Math.random() > 0.5) {
                    this.direction = -1;
                    this.node.scaleX = 1;
                }
                else {
                    this.direction = 1;
                    this.node.scaleX = -1;
                }
            }
        }
        if (this.state != 'stop') {
            var random = Math.random();
            if (random > 0.67) {
                this.scheduleOnce(this.idle_behavior, time);
            }
            else if (random > 0.33) {
                this.scheduleOnce(this.run_behavior, time);
            }
            else {
                this.scheduleOnce(this.eat_behavior, time);
            }
        }
    };
    Deer_female.prototype.eat_behavior = function () {
        var time = Math.random() * 1.5 + 2.67;
        if (this.behavior != 'eat') {
            this.anim.play('deer_eat_female');
            this.behavior = 'eat';
        }
        if (this.state != 'stop') {
            var random = Math.random();
            if (random > 0.67) {
                this.scheduleOnce(this.idle_behavior, time);
            }
            else if (random > 0.33) {
                this.scheduleOnce(this.walk_behavior, time);
            }
            else {
                this.scheduleOnce(this.run_behavior, time);
            }
        }
    };
    //private death_behavior(){}
    Deer_female.prototype.onBeginContact = function (contact, self, other) {
        if (other.node.name == 'rabbit' || other.node.name == 'Player' || other.node.name == 'bear' || other.node.name == 'wild_boar') {
            contact.disabled = true;
        }
    };
    __decorate([
        property(cc.Node)
    ], Deer_female.prototype, "Player", void 0);
    __decorate([
        property()
    ], Deer_female.prototype, "limit", void 0);
    __decorate([
        property()
    ], Deer_female.prototype, "width", void 0);
    __decorate([
        property()
    ], Deer_female.prototype, "height", void 0);
    __decorate([
        property()
    ], Deer_female.prototype, "x_offset", void 0);
    __decorate([
        property()
    ], Deer_female.prototype, "y_offset", void 0);
    Deer_female = __decorate([
        ccclass
    ], Deer_female);
    return Deer_female;
}(cc.Component));
exports.default = Deer_female;

cc._RF.pop();